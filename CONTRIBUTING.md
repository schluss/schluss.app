## Reporting Bugs

Please file bugs in the [GitLab Issue
Tracker](https://gitlab.com/schluss/schluss.app/issues).
Include at least the following in your issue report:   

 - What happened   
 - What did you expect to happen instead of what *did* happen, if it's
   not crazy obvious   
 - What version of OS and Browser you are running   
 - Screenshot if the issue concerns something visible in the GUI   
 - Console log entries, where possible and relevant   

## Contributing Code

Every contribution is welcome. If you want to contribute but are unsure
where to start, any open issues are fair game!