<style>

	#doneOverlay
	{
		position:fixed;
		top:0;
		left:0;
		height:0%;
		width:100%;
		display: table;
		
		opacity: 0;
		background-color: #efeff1;
		
		z-index:999;
		
		overflow: hidden; 
		transition: 0.75s;	
	}
	
	#doneOverlay div
	{
		display: table-cell;
		vertical-align: middle;
		text-align: center;
		
		opacity:0;
		transition: 1s;
		transition-delay:0.75s;
	}
	
	#doneOverlay.show
	{
		opacity: 0.95;
		height:100%;
	}
	
	#doneOverlay.show div
	{
		opacity:1;
	}

	.skeleton
	{
	
		-ms-transform: rotate(20deg); /* IE 9 */
		-webkit-transform: rotate(20deg); /* Safari */
		transform: rotate(20deg);
		
		position:fixed;

		right:-100px;
		top:30%;

	}

</style>

<div id="doneOverlay">
	<div class="frame">	
		<img src="assets/img/logo-pink-open.svg" alt="logo" width="150"/>
		<br/>
		<br/>
		<h1>Gelukt! Je hebt je gegevens gedeeld.</h1>
	</div>
</div>

<div class="skeleton">
	<img src="assets/img/skeleton-card.svg" alt="card" />
</div>


<header>

</header>



<main>

	

	<div class="frame">
	

	
		<div class="row centered">
		
			<div class="col">
			
				
			
				<img src="assets/img/logo-pink.svg" alt="logo" width="150"/>
			
				
				
			</div>
			
		</div>
	
		
	
	</div>

</main>


<footer>
	<div class="centered">

	<p class="waiting"><span></span><span></span><span></span></p>

	<p><b>
		Even wachten. Je deelt nu je gegevens.</b>
	</p>

	</div>
</footer>