<style>
.pinlogin .pinlogin-field
{
	box-sizing:border-box;
	
	display:inline-block;
	
	width: 40px;
	height: 40px;
	
	padding: 5px;
	
	margin: 0 5px;
	
	/*border:1px solid #fdadbc;*/
	
	border:0;
	
	-webkit-border-radius:2px;
	-moz-border-radius:2px;	
	border-radius: 2px;
	
	text-align:center;
	
	background:#fb637e;
	
	color:#fff;	
	
	font-size:1.5em;
	font-weight:bold;
	line-height:40px;

}

.pinlogin .pinlogin-field:first-of-type {
    margin-left: 0;
}

.pinlogin .pinlogin-field:last-of-type {
    margin-right: 0;
}

.pinlogin .pinlogin-field:read-only
{
	border-color:#fb637e;
	color:#fff;
	cursor:default;
}

.pinlogin .pinlogin-field.invalid
{
	border:1px solid red;
	color:red;
}


/* override arrow position of the tooltip */
.tooltip .tooltiptext::after {
	right: 25px;
}

</style>


			
	<header>
		<div class="frame centered">
		
			<div class="logo">
				<img src="assets/img/logo-white.svg" alt="logo" width="49"/>
			</div>
		
		</div>  
	</header>

	<main>
	<form action="#/register" id="registerForm" method="post" novalidate autocomplete="off">
	<input type="hidden" name="pin" id="pin" /> 	
		<div class="frame">
		
			<div class="centered">
				<h1>Kies een pincode die je <u>niet vergeet</u>&nbsp;
					<div id="ttip" class="tooltip" data-tooltip="Alleen jij hebt de code. Zelfs Schluss weet 'm niet"><img src="/assets/img/tooltip.svg" alt="tooltip" /></div>
				</h1>
				
				<p class="light"><img src="assets/img/lock.svg" alt="lock" /> Je pincode en gegevens zijn veilig. Door gebruik van Schluss-encryptie.</p>
			
			
			<br/>
			
				
			<label for="pin">Kies een 5-cijferige pincode:</label>
			<div id="pinwrapper" class="centered"></div>
			
			<br/>
			
			<label for="pinrepeat">Herhaal je pincode:</label>
			<div id="pinrepeatwrapper" class="centered"></div>
			
			<br/>
			
			<!--<p class="light">By signing up you agree to the <a href="#/terms">terms</a></p>-->
			
			
			</div>		
		</div>

	</form>
	</main>

	<footer>

		<div class="frame centered">
		<br/>
			<button id="registerSubmit" disabled="disabled" type="submit">Maak kluis aan</button>
		</div>

	</footer>

