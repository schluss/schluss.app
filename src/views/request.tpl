<style>
	.overlay
	{
		position:fixed;
		top:-1000;
		left:0;
		height:0%;
		width:100%;
		display: table;
		
		opacity: 0;
		background-color: #efeff1;
		
		z-index:999;
		
		overflow: hidden; 
		transition: 0.75s;	
	}
	
	.overlay .overlaycontainer
	{
		display: table-cell;
		vertical-align: middle;
		text-align:center;
		width:100%;
		opacity:0;
		transition: 0.5s;
	}
	
	.overlay.show
	{
		top:0;
		opacity: 0.95;
		height:100%;
	}
	
	.overlay.show .overlaycontainer
	{
		opacity:1;
	}
	
	/* Longpress button */
	button.longpress
	{
		background-size: 200% 100%;
		background-image: linear-gradient(to right, #f88499 50%, #fa4a69 50%);
		-webkit-transition: background-position 0.3s;
		-moz-transition: background-position 0.3s;
		transition: background-position 0.3s;	
		transition-timing-function: linear;	
		
		-webkit-touch-callout: none;
		-webkit-user-select: none;
		-khtml-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		-webkit-tap-highlight-color: rgba(0,0,0,0);
		-webkit-tap-highlight-color: transparent;
	}
			
	button.longpress.pressing
	{
		background-position: -100% 0;
		transition: background-position 3s;
		transition-timing-function: linear;
	}
	
	button.longpress.pressed
	{
		background:#fa4a69;
	}
	
	button.longpress.shake
	{
		animation: shake 0.82s cubic-bezier(.36,.07,.19,.97) both;
		transform: translate3d(0, 0, 0);
	}
	
	@keyframes shake {
	  10%, 90% {
		transform: translate3d(-1px, 0, 0);
	  }
	  
	  20%, 80% {
		transform: translate3d(2px, 0, 0);
	  }

	  30%, 50%, 70% {
		transform: translate3d(-4px, 0, 0);
	  }

	  40%, 60% {
		transform: translate3d(4px, 0, 0);
	  }
	}	
	
/* override arrow position of the tooltip */
.tooltip .tooltiptext::after {
	right: 25px;
}	
</style>

<div id="redirectOverlay" class="overlay">
	<div class="overlaycontainer centered">

		<img src="assets/img/logo-pink.svg" alt="logo" width="49"/>
		
		<br/><br/>
		
		<div class="frame">
		<div class="card">
		
		<div class="row">
		<div class="col">
		<h1>Je verlaat nu<br/>www.schluss.app</h1>
		
		<hr />
		
		<p>En gaat naar<br/>
		<img src="/assets/img/arrow-pink-right.svg" alt="arrow right"/> <span id="redirecturlname" style="color:#fb637e;font-weight:bold;"></span>
		</p>
		
		</div>
		</div>
		
		</div>
		</div>

	</div>
</div>

<div id="abortOverlay" class="overlay">
	<div class="overlaycontainer">

		<img src="assets/img/logo-pink.svg" alt="logo" width="49"/>
		
		<br/><br/>
		
		<div class="frame">
		<div class="card">
		
		<div class="row">
		<div class="col left">
		<h1 class="left">Weet je zeker dat je dit dataverzoek wilt afbreken?</h1>
		
		<p>Je kunt het verzoek later altijd terugvinden op je dashboard </p>
		<br/>
		<hr />
		
		</div>
		</div>
		
		<div class="row">
		<div class="col half" style="padding-top:8px;">
		<a href="javascript:void(0);" id="postponeCancelLink">Nee, ga terug</a>
		</div>
		<div class="col half">
		<button type="button" class="small" id="postponeConfirmLink">Ja, afbreken</button>
		</div>
		</div>
		
		</div>
		</div>

	</div>
</div>

<header>
	<div class="frame">
		<div class="logo" style="float:left;">
			<img src="assets/img/logo-pink.svg" alt="logo" width="49" />
		</div>
		
		<div style="float:right;">
			<a href="javascript:void(0)" id="postponeLink">Later verder &nbsp; <img src="/assets/img/abort.svg" alt="abort" style="vertical-align:middle;" /></a>
		</div>
	</div> 
</header>

<main>

	<div class="frame">
	
		<h1>Dataverzoek&nbsp;<div id="ttip" class="tooltip" data-tooltip="Dataverzoek tooltip"><img src="/assets/img/tooltip.svg" alt="tooltip" /></div></h1>
		
		<br/>
		
		<div class="card">
		
			<div class="cardheader arc_grey_down">
		
				<div class="row">
					<div class="col onesixth">
						<img src="<%= provider.logourl %>" alt="logo" style="width:22px;height:22px;border-radius:22px;background:#fff;padding:5px;" /> 	
					</div>
					<div class="col fivesixth">
						<p>
							<b><%= provider.name %></b><br/>
							<%= scope.name %>
						</p>					
					</div>
				</div>
			</div>
			
			<div class="row">
			
				<div class="col">
					<p>Wil graag bekijken:</p>
				</div>
			
			</div>
			
			<hr />
			
			<div class="row">
			
				<div class="col onesixth">
					<img src="/assets/img/checkbox-checked-disabled.svg" id="checkboxOff" />
					<img src="/assets/img/checkbox-checked.svg" id="checkboxOn" />
				</div>
				<div class="col half">
					<span class="requestProviderName"><%= scope.request %></span>
					<div id="requestVia"></div>
				</div>
				<div class="col third right">
					<a href="#" id="getContentBtn">Ophalen</a>
				</div>
			</div>
			
			<hr />
			
			<div class="row" id="period">
				<div class="col">
					<br/>
					<p><img src="/assets/img/calendar.svg" alt="calendar" /> Eenmalig bekijken</p>
				</div>
				
			</div>
	
		</div>

	
	
	</div>
		
</main>

<footer>
	<div class="centered">
		<button type="button" name="processBtn" id="processBtn" class="longpress">Akkoord</button>
		<br/><br/>
		<p class="light" id="longpressMsg">Houd de knop 3 seconden vast</p>
	</div>
</footer>