<header>
	<div class="frame">
		<div class="logo centered">
			<img src="assets/img/logo-pink.svg" alt="logo" width="49"/>
		</div>
	</div> 
</header>



<main>

	<div class="frame">
	
		<div class="row">
		
		<div class="col centered">
	
		<h1><span id="companyname"></span> heeft inzage gekregen!</h1>
		
		<p>Je kunt nu uitloggen of verder werken aan je digitale kluis.	</p>
		
		<br/>
		<br/>
		
		<img src="/assets/img/maus-handshake.png" width="100%" alt=""/>
		
		<br/>
		
		</div>
	
	</div>
</div>
	

</main>

<footer>
	<div class="centered">
		<button type="button" onclick="window.location.href='#/'">Bekijk je inzages</button>
		
		<br />
		<br />
		
		<a href="/#logout"><img src="/assets/img/logout-grey.svg" width="20" alt="sign out"/> Uitloggen</a>
	</div> 
</footer>