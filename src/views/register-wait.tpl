	<style>
		.panelcontainer {
		  --n: 1;
		  display: flex;
		  align-items:center;
		  overflow-y: hidden;
		  width: 100%;
		  width: calc(var(--n)*100%);
		 
		  /*height: 50vw;
		  max-height: 100vh;	*/
		  
		  transform: translate(calc(var(--tx, 0px) + var(--i, 0)/var(--n)*-100%));
		}
		.panel {
			width: 100%;
			width: calc(100%/var(--n));
			user-select: none;
			pointer-events: none;
			cursor:pointer;
			
			
			padding-left:2em;
			padding-right:2em;
		}
		
		.panel img
		{
			width:100%;
			max-width:150px;
		}

		.smooth {
			transition: transform calc(var(--f, 1)*.5s) ease-out;
		}
		
		.progress
		{
			display:inline-block;
			width:8px;
			height:8px;
			background:#e2e1e9;
			border-radius:100%;
			margin-right:7px;
			
			transition: background-color 0.2s linear;
		}
		
		.progress.on
		{
			background:#586071;
		}
		
		
		#doneOverlay
		{
			position:fixed;
			top:0;
			left:0;
			height:0%;
			width:100%;
			display: table;
			
			opacity: 0;
			background-color: #efeff1;
			
			z-index:999;
			
			overflow: hidden; 
			transition: 0.75s;	
		}
		
		#doneOverlay div
		{
		    display: table-cell;
			vertical-align: middle;
			text-align: center;
			
			opacity:0;
			transition: 1s;
			transition-delay:0.75s;
		}
		
		#doneOverlay div img
		{
			padding:15px;
			border-radius:100%;
			border:3px solid #fa4a69;
		}
		
		#doneOverlay.show
		{
			opacity: 0.95;
			height:100%;
		}
		
		#doneOverlay.show div
		{
			opacity:1;
		}

	</style>	

<div id="doneOverlay">
	<div>	
		<img src="/assets/img/check.svg" width="50" />
		<br/>
		<br/>
		<h1>Je kluis is<br/>klaar</h1>
	</div>
</div>

<header class="ovalbeloww lightgrey arc_lightgrey_down">
	<div class="centered">
			
		<p class="waiting">Kluis aanmaken<br/><span></span><span></span><span></span></p>
	
	</div> 
</header>


<main>
	<div class="centered panelcontainer" id="panelcontainer">
	
		<!--<div id="panelcontainer" class="panelcontainer">-->
	
			<div id="panel1" class="panel row">
			
				<div class="col">	

					<h1>Je kluis is in de maak.<br/>Keep calm...</h1>
				
					<p>Lees ondertussen meer over de beveiliging van je gegevens.</p>
				
					<br/>
					<br/>
					
					<img src="/assets/img/maus-lockswing.png" alt="" />
				</div>

			</div>
			
			<div id="panel2" class="panel row">
			
				<div class="col">
				
			
				<h1>Jouw gegevens zijn veilig</h1>
				
				<p>Schluss is er voor jou. Zodat jij bepaalt met wie je je gegevens deelt.</p>
				
				<br/>
				<br/>
				
				<img src="/assets/img/maus-contents.png" alt="" />
				
				</div>

			</div>	

			<div id="panel3" class="panel row">
			
				<div class="col">
	
				<h1>Jouw gegevens zijn van jou</h1>
				
				<p>Bij Schluss ben jij de baas over je gegevens. Daarom slaat Schluss je gegevens niet op.</p>
				
				<br/>
				<br/>
				
				<img src="/assets/img/maus-carryon.png" alt="" />
				</div>

			</div>	

			<div id="panel4" class="panel row">
			
				<div class="col">
				
				
				<h1>Check het zelf</h1>
				
				<p>Check of voor je gegevens die je deelt gebruik wordt gemaakt van Schluss-encryptie.</p>
				
				<br/>
				<br/>
				
				<img src="/assets/img/maus-readlaxed.png" alt="" />
				
				</div>

			</div>				
		
		<!--</div>-->
	
	</div>
</main>

<footer>
		<div class="centered">
			<div id="panelprogress">
				<span id="progress0" class="progress"></span>
				<span id="progress1" class="progress"></span>
				<span id="progress2" class="progress"></span>
				<span id="progress3" class="progress"></span>
			</div>
		</div>
</footer>