<style>
.pinlogin .pinlogin-field
{
	box-sizing:border-box;
	
	display:inline-block;
	
	width: 40px;
	height: 40px;
	
	padding: 5px;
	
	margin: 0 5px;
	
	/*border:1px solid #fdadbc;*/
	
	border:0;
	
	-webkit-border-radius:2px;
	-moz-border-radius:2px;	
	border-radius: 2px;
	
	text-align:center;
	
	background:#fb637e;
	
	color:#fff;	
	
	font-size:1.5em;
	font-weight:bold;
	line-height:40px;

}

.pinlogin .pinlogin-field:first-of-type {
    margin-left: 0;
}

.pinlogin .pinlogin-field:last-of-type {
    margin-right: 0;
}

.pinlogin .pinlogin-field:read-only
{
	border-color:#fb637e;
	color:#fff;
	cursor:default;
}

.pinlogin .pinlogin-field.invalid
{
	border:1px solid red;
	color:red;
}

</style>

			
	<header>
		<div class="frame centered">
		
			<div class="logo">
				<img src="assets/img/logo-white.svg" alt="logo" width="49"/>
			</div>
		
		</div>  
	</header>

	<main>
	<form action="#/login" method="post" id="loginForm" autocomplete="off">
	<input type="hidden" name="pin" id="pin" /> 	
		<div class="frame">
		
			<div class="centered">
				<h1 id="loginTitle">Welkom bij Schluss</h1>
		
				<p class="light">Login met je pincode</p>
			</div>
			
			<br/>
				
			<div id="pinwrapper" class="centered"></div>
					
		</div>
	</form>
	</main>

	<footer class="centered">
		<!--<p class="light"><a href="#/welcome">Account aanmaken</a></p>-->
	</footer>
