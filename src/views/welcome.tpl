	<style>
		.panelcontainer {
		  --n: 1;
		  display: flex;
		  align-items:center;
		  overflow-y: hidden;
		  width: 100%;
		  width: calc(var(--n)*100%);
		 
		  /*height: 50vw;
		  max-height: 100vh;	*/
		  
		  transform: translate(calc(var(--tx, 0px) + var(--i, 0)/var(--n)*-100%));
		}
		.panel {
			width: 100%;
			width: calc(100%/var(--n));
			user-select: none;
			pointer-events: none;
			cursor:pointer;
			
			
			padding-left:2em;
			padding-right:2em;
		}
		
		.panel img
		{
			width:100%;
			max-width:150px;
		}

		.smooth {
			transition: transform calc(var(--f, 1)*.5s) ease-out;
		}
		
		.progress
		{
			display:inline-block;
			width:8px;
			height:8px;
			background:#e2e1e9;
			border-radius:100%;
			margin-right:7px;
			
			transition: background-color 0.2s linear;
		}
		
		.progress.on
		{
			background:#586071;
		}

		.testt
		{
			position:fixed;
			z-index:99;
			height:45vh;
			top:0;
			left:0;
			width:100%;
			background:#efeff1;
		}

	</style>	

<div class="testt lightgrey arc_lightgrey_down"></div>

<header class="lightgrey">
	<div class="centered">
	
		<div class="logo">
			<img src="assets/img/logo-pink.svg" alt="logo" width="49"/>
		</div>
	
	</div>  
</header>

<main>
	<div class="centered panelcontainer" id="panelcontainer">
	

			<div id="panel1" class="row panel">
			
				<div class="col">					
				
				<img src="assets/img/maus-mobile-control.png" alt=""/>
				
				<br/>				
				<br/>
				<br/>
				
				<h1>Welkom bij Schluss</h1>
				
				<p>
					Al je gegevens veilig opgeborgen.<br/> 
					Alleen jij bepaalt met je ze deelt.<br/>
					Makkelijk en snel.
				</p>
				
				</div>

			</div>
			
			<div id="panel2" class="panel row">
			
				<div class="col">
				
				<img src="assets/img/maus-head.png" alt=""/>
				<br/>				
				<br/>
				<br/>
				
				<h1>Jij bent de baas</h1>
				
				<p>Bepaal zelf wie wat wanneer over jou mag weten.</p>
				
				</div>

			</div>	

			<div id="panel3" class="panel row">
			
				<div class="col">
					<img src="assets/img/maus-key-secure.png" alt=""/>
					
					<br/>				
					<br/>	
					<br/>
					
					<h1>Veilig</h1>
					<p>Bewaar en deel je gegevens veilig met je digitale kluis: alleen jij hebt de code.</p>
				</div>

			</div>	

			<div id="panel4" class="panel row">
			
				<div class="col">
					<img src="assets/img/maus-handshake.png" alt=""/>
					
					<br/>				
					<br/>	
					<br/>	
					
					<h1>Makkelijk en snel</h1>
					
					<p>Deel je gegevens makkelijk en snel veilig met bedrijven, instanties, familie en vrienden.</p>
				</div>

			</div>				
	
	</div>
</main>

<footer>
		<div class=" centered">

			<a href="#/our-promise" class="button">Aan de slag</a>

			<div id="panelprogress">
				<span id="progress0" class="progress"></span>
				<span id="progress1" class="progress"></span>
				<span id="progress2" class="progress"></span>
				<span id="progress3" class="progress"></span>
			</div>
		</div>
</footer>