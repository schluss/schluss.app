<script>
		$('#hamburger').click(function(){
			$('#mainnav').toggleClass('show');
		});
</script>

<header>

	<div class="frame">
	
		<div class="logo" style="float:left;">
			<img src="assets/img/logo-pink.svg" alt="logo" width="49" />
		</div>
		
		<nav id="mainnav">
		
			<div id="slider">
			
				<div class="logo">
					<a href="#/"><img src="assets/img/logo-white.svg" alt="logo" width="49" /></a>
				</div>			
			
				<ul class="">
					<li><a href="#/">Dashboard</a></li>
					<li><a href="#/data">Gegevens</a></li>
					<li><a href="#/shares">Inzages</a></li>
				</ul>
				
				<ul class="smaller">
					<li><a href="https://www.schluss.org" target="_blank"><img src="assets/img/support.svg" alt="logo" width="20"/> Support</a></li>
					<li><a href="#/logout"><img src="assets/img/logout.svg" alt="logout" width="20" /> Uitloggen</a></li>
				</ul>
			</div>
			
			
			<div id="hamburger">
				<span></span>
				<span></span>
				<span></span>		
			</div>
			
		
		</nav>
	
	</div>

</header>


<main>

	<div class="frame">

	<div class="row">
		<div class="col">
			<h1><%= index.name %></h1>
		</div>
	</div>
	
	<div class="card">
	
		<div class="row">
			<div class="col">
			<%! content %>
			</div>
		</div>
	
	</div>
	
</main>

<footer>
		<div class="centered">
			<a href="#/data" class="button">Terug</a>
		</div>
</footer>