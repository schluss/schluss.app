<header>
	<div class="centered">
	
		<div class="logo">
			<img src="assets/img/logo-pink.svg" alt="logo" width="49"/>
		</div>
	
	</div>  
</header>

<main>

	<div class="frame">
	
		<h1>Fijn dat je je aanmeldt bij Schluss!</h1>
		
		<br/>
		
		<div class="card">
		
			<div class="row">
			
				<div class="col">
		
					<h2>We beloven je:</h2>
						
					<ul class="checks">
					 <li>Jij blijft altijd de baas over je gegevens</li>
					 <li>Jij bepaalt wie jouw gegevens mag zien</li>
					 <li>Niemand kan zonder jouw toestemming bij je gegevens, zelfs Schluss niet.</li>
					 
					</u>
					
					<br/>
					
					<p>
					<p>Door je aan te melden bij Schluss, ga je akkoord met onze (korte, ja echt) <a href="#/terms">algemene voorwaarden en privacyverklaring</a>.</p>
					</p>
					
				</div>
				
			</div>
			
		</div>

	
	
	</div>
		
</main>

<footer class="oval">
		<div class="centered">
			<a href="#/register" class="button">Ga verder</a>
		</div>
</footer>