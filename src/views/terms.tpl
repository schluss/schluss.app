<header>
	<div class="centered">
	
		<div style="position:absolute;left:2em;top:25px;"><a href="#/our-promise"><img src="assets/img/arrow-grey-left.svg" style="vertical-align:middle;" alt=""/>&nbsp; Terug</a></div>
		
		<div class="logo">
			<img src="assets/img/logo-pink.svg" alt="logo" width="49"/>
		</div>
	
	</div>  
</header>

<main>

	<div class="frame">
	
		<br/>
	
		<h1>Terms</h1>
		<p><b>1. General...</b></p>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi aliquam facilisis dui. Duis vitae ante auctor, mollis ipsum sit amet, pulvinar enim. Nunc interdum urna vel mauris posuere, sit amet faucibus lacus imperdiet. Nulla non ligula a eros pulvinar molestie vel id lectus. Ut a maximus odio, sit amet molestie felis. Quisque pulvinar non massa non elementum. Curabitur scelerisque risus vel neque gravida dictum. Nunc condimentum quam id felis efficitur, eu hendrerit orci fermentum. Duis ultricies molestie massa, ornare bibendum quam volutpat et. Etiam mauris leo, blandit in faucibus sed, vehicula vitae lacus. Vivamus viverra viverra ornare. Mauris tortor est, vehicula vel tempor et, aliquam vel dolor. Praesent elit sem, lobortis lobortis luctus mollis, dapibus non mauris. Donec vel libero vitae ex tempor ultricies.</p>
		<br/>
		<p><b>2. More...</b></p>
		<p>In quis dui vitae nunc finibus gravida. Curabitur molestie pharetra tellus id dapibus. Cras rhoncus libero at felis viverra auctor. Vivamus efficitur condimentum nibh. Quisque elementum dignissim vulputate. Maecenas eu molestie nulla. Curabitur ante tortor, tempus non dui sodales, finibus consequat diam. Donec a cursus velit, sit amet vehicula leo.</p>
		<br/>
		<p><b>3. And more...</b></p>
		<p>Integer hendrerit, nisl nec fermentum porttitor, lectus lectus suscipit nunc, id suscipit leo dolor vel lacus. Nulla velit sem, aliquam non gravida eleifend, dignissim vel ipsum. Ut semper consectetur ligula, tempus varius nibh aliquet in. Praesent interdum risus in nisi hendrerit, at semper elit pretium. Aenean sed lectus posuere tellus eleifend commodo. Integer lacinia justo non ultrices hendrerit. Aliquam erat volutpat. Vestibulum magna mi, suscipit eget interdum non, volutpat ac tellus. Vestibulum vestibulum libero vel lectus viverra fringilla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras gravida, magna eget eleifend scelerisque, nisi felis rutrum mauris, sit amet semper felis justo convallis leo. Proin scelerisque egestas dignissim. Maecenas vitae massa venenatis, vestibulum eros eu, hendrerit tortor. Mauris ornare scelerisque neque a bibendum. Praesent nec purus nulla. Fusce consectetur tellus lacus, ut efficitur libero mollis nec.</p>		
		<br/>
		<p><b>4. And more...</b></p>
		<p>In hac habitasse platea dictumst. Phasellus et libero quam. Quisque auctor imperdiet dui ac cursus. Sed dignissim nec augue at dapibus. Maecenas sit amet venenatis ante. Proin vulputate tortor at magna hendrerit, a fermentum neque consequat. Curabitur accumsan mi sit amet turpis tincidunt feugiat. Morbi quam turpis, porttitor sit amet nulla sed, mollis gravida nunc. Quisque imperdiet sapien quis nulla faucibus lobortis. Nam nec lacinia diam. Suspendisse at laoreet nibh. Phasellus elementum viverra risus vitae ultricies. Phasellus lobortis, nisi in hendrerit viverra, justo nisi finibus nunc, et fringilla nunc quam id risus. Praesent at diam posuere, eleifend enim eu, congue lacus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nunc id pulvinar quam, in posuere metus.</p>
	</div>
</main>

<footer>
	
	<div class="frame">		

		<br/>
		<a href="#/our-promise"><img src="assets/img/arrow-grey-left.svg" style="vertical-align:middle;" alt=""/>&nbsp; Terug</a>

	</div>

</footer>