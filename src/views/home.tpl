<script>
		$('#hamburger').click(function(){
			$('#mainnav').toggleClass('show');
		});
		
		$('#idinbtn').click(function(){
			window.location.href='#/idin/about';
		});
</script>

<header>

	<div class="frame">
	
		<div class="logo" style="float:left;">
			<img src="assets/img/logo-pink.svg" alt="logo" width="49" />
		</div>
		
		<nav id="mainnav">
		
			<div id="slider">
			
				<div class="logo">
					<a href="#/"><img src="assets/img/logo-white.svg" alt="logo" width="49" /></a>
				</div>			
			
				<ul class="">
					<li><a href="#/">Dashboard</a></li>
					<li><a href="#/data">Gegevens</a></li>
					<li><a href="#/shares">Inzages</a></li>
				</ul>
				
				<ul class="smaller">
					<li><a href="https://www.schluss.org" target="_blank"><img src="assets/img/support.svg" alt="logo" width="20"/> Support</a></li>
					<li><a href="#/logout"><img src="assets/img/logout.svg" alt="logout" width="20" /> Uitloggen</a></li>
				</ul>
			</div>
			
			
			<div id="hamburger">
				<span></span>
				<span></span>
				<span></span>		
			</div>
			
		
		</nav>
	
	</div>

</header>


<main>

	<div class="frame">

	<div class="row">
		<div class="column">
			<h1>Hallo, <br/><span id="displayname"></span></h1>
		</div>
	</div>
	<!--
	<div class="card">
	
	
		<div class="row">
			<div class="col onesixth">
				<img src="" alt="logo" style="width:22px;border-radius:22px;background:#fff;padding:5px;" /> 	
			</div>
			<div class="col fivesixth">
				<p>
					<b></b><br/>
					
				</p>					
			</div>
		</div>
				
	</div>
	-->
	<div id="notificationArea"></div>
	
	<!--
	<div class="card tip">
		<div class="row"> 
			<div class="col fivesixth">
				<h3><span class="highlight"><b>Tip:</b></span> Plaats je identiteit in de kluis!</h3>
			</div>
			<div class="col onesixth right">
				<br/>
				<button type="button" class="round" id="idinbtn"><img src="/assets/img/arrow-white-right.svg" alt="arrow pointing right" /></button>
			</div>
		</div>
	
	</div>
	-->

	<div class="row">
	<div class="column">
	
	
	<div id="displayRequests" style="display:none;float:right;margin-top:-5px;"><i class="fas fa-exclamation-circle" style="color:red;"></i></div>
	
		<div id="requests"></div>
	
		<div id="contextdata"><ul></ul></div>
	
	</div>
	</div>
	
	</div>
	
</main>

<footer>
</footer>