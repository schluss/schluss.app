<header>
	<div class="frame">
		<div class="logo">
			<img src="assets/img/logo-pink.svg" alt="logo" width="49" />
		</div>
	</div> 
</header>

<main>

	<div class="frame centered">
	
		<h1>Dataverzoek</h1>
		
		<br/>
		
		<img src="assets/img/skeleton-card.svg" alt="card" />

	</div>

</main>


<footer>
	<div class="frame centered">

		<p class="waiting"><span></span><span></span><span></span></p>
		<br/>
		<p>Even wachten. De gegevens worden in je kluis geplaatst...</p>

	</div>
</footer>