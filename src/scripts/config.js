module.exports = {
	persistentSession : true,
	
	debug : true,
	
	ipfs : {
		host : 'ipfs.infura.io', // "ipfs.schluss.app"
		port : 5001,			 // 443
		protocol : 'https'
		
	}
};

/*
module.exports = function() {
	return {
		persistentSession : true,
		
		debug : true,
		
		ipfs : {
			host : 'ipfs.infura.io', // "ipfs.schluss.app"
			port : 5001,
			protocol : 'https'
			
		}
	};
}
*/