global.tools = require('../tools.js');
global.provider = require('../provider.js');
//global.context = require('../context.js');

module.exports = {
    // view a request
    getrequest: function (e) {

        async function runasync() {

			provider.request = await db.get('requests', e.params['id']);

            await provider.init(provider.request.settingsuri);


            let scope = await provider.getScope(provider.request.scope);
			
			// get users' public key hash
			let pgpKeyPair = await db.get('app','pgpkey');

            // generate the right redirect to third party url (the one that offers the data)
            scope.settings.endpoints.connect = scope.settings.endpoints.connect.replace('[settingsurl]', encodeURIComponent(provider.request.settingsuri));
            scope.settings.endpoints.connect = scope.settings.endpoints.connect.replace('[scope]', encodeURIComponent(scope.scope));
            scope.settings.endpoints.connect = scope.settings.endpoints.connect.replace('[token]', encodeURIComponent(e.params['id'])); // todo: make unique token for use only between schluss.app and third party (duo)
            //scope.settings.endpoints.connect = scope.settings.endpoints.connect.replace('[pubkey]', encodeURIComponent(pgpPublicKey)); 

            // returnurl:
            var returnurl = window.location.origin + '#/returnrequest/' + e.params['id'];
            scope.settings.endpoints.connect = scope.settings.endpoints.connect.replace('[returnurl]', encodeURIComponent(returnurl));

			// todo: send ipfs-hash (or use keyserver construction) to send the users key to third party
			scope.settings.endpoints.connect += '&pubkeyhash=' + encodeURIComponent(pgpKeyPair.pubkeyhash);

            tools.log('redirect url:');
            tools.log(scope.settings.endpoints.connect);
			
            await e.partial('views/request.tpl', { provider: provider.settings, scope: scope, id: e.params['id'] }, function (t) {

                tools.setTheme('grey');
                tools.setLayout('wizard');

                // add / enable tooltip for ttip element
                tools.tooltip('ttip');

                // manage cancel links/buttons
                $('#postponeLink').click(function () {
                    $('#abortOverlay').addClass('show');

                    $('#postponeCancelLink').click(function () {
                        $('#abortOverlay').removeClass('show');
                    });

                    $('#postponeConfirmLink').click(function () {
                        e.redirect('#/');
                    });
                });

                // for now: 1 single source only possible, future must be able to contain multiple
                // when is filled
                if (provider.request.isstored === true) {
                    //$('#payload').html(provider.request.payload);
                    //$('#payload').show();

                    $('#getContentBtn')
                        .html('<img src="/assets/img/check-green.svg" /> In kluis')
                        .attr('href', '#')
                        .addClass('inlinebutton green')
                        .show();

                    $('#checkboxOff').hide();
                    $('#checkboxOn').show();

                    $('#processBtn').removeAttr('disabled');
                    $('#longpressMsg').show();

					// determine this is not a contract where data needs to be returned to the requesting party
					// we know this (for now) by knowing the config.json (from originating party = provider) does (not) have a return enddpoint
					
					tools.log('check:');
					tools.log(provider.settings.endpoints);
					
					// when not to forward payload:
					if (provider.settings.endpoints.return == undefined || provider.settings.endpoints.return == "")
					{
						// hide some elements
						document.getElementById('period').style.display = 'none'; // period overview
						document.getElementById('longpressMsg').style.display = 'none'; // longpressbutton message
						
						
						let btn = document.getElementById('processBtn');
						btn.innerHTML = 'Ok';
						btn.classList.remove('longpress');
						//btn.classList.add('button')
						btn.addEventListener('click', function(){
							
							// delete open request to wrap things up
							db.remove('requests', e.params['id']);							
									
							//alert ('test');
							
							e.redirect('#/data');
							return;
							
						});
						
					}
					// when to forward payload to third party
					else
					{
						// Logic for longpress button - future: remove from app.js
						// in this case, already removed jquery dependencies


						let btn = document.getElementById('processBtn');
						let timer, start, end, pressTime = 3000;

						// when mousedown / touchstart
						btn.onPress = function () {
							start = new Date();

							btn.classList.add('pressing');

							// add x sec pressing time timer
							timer = setTimeout(function () {

								// cleanup and reset
								btn.classList.remove('pressing');
								btn.classList.add('pressed');
								btn.disabled = true; // disable the button

								// call onPressed handler, if available
								if (btn.onPressed)
									btn.onPressed();

							}, pressTime);
						}

						// when mouseup / touchend
						btn.onPressCancel = function () {

							end = new Date();

							btn.classList.remove('pressing');

							clearTimeout(timer);

							// when button is released to quick, shake it (with a max of 1 sec after pressing)
							if (end - start <= 1000) {
								btn.classList.add('shake');

								setTimeout(function () {
									btn.classList.remove('shake');
								}, 820);
							}
						}

						// attach event listeners

						// mouse
						btn.addEventListener('mousedown', btn.onPress);
						btn.addEventListener('mouseup', btn.onPressCancel);

						// touch
						btn.addEventListener("touchstart", btn.onPress, { passive: true });
						btn.addEventListener("touchend", btn.onPressCancel);

						// handler for what to do when user pressed the button for x time
						btn.onPressed = function () {
							// submit the form
							//document.getElementById('processForm').submit();
							e.redirect('#/completerequest/' + e.params['id']);
							
						}

						// End logic for longpress button
					}
                }
                // when not filled
                else {
                    //$('#payload').hide();

                    $('#getContentBtn')
                        .attr('href', '#')
                        .addClass('inlinebutton pink')
                        .html('Ophalen')
                        .click(function () {

                            // show overlay that user s beeing redirect externally
                            $('#redirectOverlay').addClass('show');

                            //$('#redirecturlname').text(tools.shorten(scope.settings.endpoints.connect, 30));
                            $('#redirecturlname').text(tools.shorten(provider.request.redirecturi, 30));

                            window.setTimeout(function () {
								
                                //window.location.href = scope.settings.endpoints.connect;
                                window.location = provider.request.redirecturi;
                            }, 2000);

                        })

                    $('#checkboxOff').show();
                    $('#checkboxOn').hide();


                    $('#processBtn').attr('disabled', 'disabled');
                    $('#longpressMsg').hide();
                }

                // when via
                //if ()
                $('#requestVia').html('<img src="/assets/img/arrow-pink-right.svg" /> <span class="pink">via: </span>' + scope.settings.name);

                tools.log(provider.request.payload);

            });

        }

        runasync();
    },

    // POST not possible without a server backend, so every return to the schluss.app will have to be done via a GET request
    getreturnrequest: function (e) {

        tools.log('id:' + e.params['id']);

        e.partial('views/request_returning.tpl', null, function (t) {

            tools.setTheme('grey');
            tools.setLayout('wizard');

            // screen wait helper: start the time
            tools.timer.start();

            async function runasync() {

				// get information about the request
				//provider.request = await context.getItem('requests', e.params['id']);
				provider.request = await db.get('requests', e.params['id']);
				await provider.init(provider.request.settingsuri);
				let scope = await provider.getScope(provider.request.scope);
				
				let pgpKeyPair = await db.get('app','pgpkey');				
				
				// Retrieve payload:
				
					// get (encrypted) payload
					// When token is in url (/url/to/go/to/[token]) it is replaced. To be sure it's also send via POST alongside the publickey
					scope.settings.endpoints.retrieve = scope.settings.endpoints.retrieve.replace('[token]', encodeURIComponent(e.params['id']));
					let payloadEncrypted = await $.post(scope.settings.endpoints.retrieve, {token :  encodeURIComponent(e.params['id']), publickey : pgpKeyPair.public});			
				
					var payload;
				
					// When explicitly defined in config.json of data provider: 
					// { "exportformat" : "plain"
					// no (pgp) encryption is used during transport. Of course data is sent securely through ssl
					if(scope.settings.exportformat != undefined || scope.settings.endpoints.exportformat == 'plain')
					{
						payload = payloadEncrypted;
						
						//tools.log(payloadEncrypted);
					}
					// When payload is encrypted using PGP
					else
					{
				
						// decrypt payload with private key:
						const options = {
							message: await openpgp.message.readArmored(payloadEncrypted.payload),    // parse armored message
							publicKeys: (await openpgp.key.readArmored(pgpKeyPair.public)).keys[0], // for verification (optional)
							privateKeys: [(await openpgp.key.readArmored(pgpKeyPair.private)).keys[0]]                                 // for decryption
						};
						
						let payloadDecrypted = await openpgp.decrypt(options);
					
						payload = payloadDecrypted.data; // contains the actual decrypted data
					}				

				
				
				var payloadJSON = JSON.parse(payload);
				
				tools.log('DECRYPTED PAYLOAD:');
				tools.log(payloadJSON);
				
				var payloadProcessed = [];
				
				// loop through requested fields
				for (var y  = 0; y < scope.fields.length; y++){
					
					var attr = { name : '', label : '', description : '', value : ''};
					
					// loop through payload fields
					for (var i = 0; i < payloadJSON.payload.length; i++){
						
						attr.name = scope.fields[y].name;
						attr.label = scope.fields[y].label;
						attr.description = scope.fields[y].description;							
						
						// match field with requested field in scope
						if (payloadJSON.payload[i].name == scope.fields[y].match){
							
							attr.value = payloadJSON.payload[i].value;
							break;
						}
					}	

					// add result to processed array
					payloadProcessed.push(attr);						

				}
				
				tools.log('Payload processed:');
				tools.log(payloadProcessed);

				// Store payload in context (personal locker):

				// 1: encrypt payload with symmetric key (retrieved from personal public key)

				let msgOptions = {
				  message : openpgp.message.fromText(JSON.stringify(payloadProcessed)),
				  publicKeys: (await openpgp.key.readArmored(pgpKeyPair.public)).keys[0],  // input as String (or Uint8Array)
				  privateKeys: [(await openpgp.key.readArmored(pgpKeyPair.private)).keys[0]] // for signing
				};
				
				let ciphertext = await openpgp.encrypt(msgOptions);

				// 2: store payload in IPFS
				let hash = await ipfs.addJSON({content : ciphertext.data, version : '0.0.1'});
				
				// 3: make and store index 
				var indexId = tools.uuidv4();

				await db.set('index', indexId, { 
					name : scope.request, 
					label : scope.request, 
					description : scope.name, 
					scope : provider.request.scope,
					logourl : scope.settings.logourl,
					hash : hash, 
					type : 'string', 
					timestamp: Math.round(new Date().getTime()/1000), 
					version : '0.0.1'
					});
					
				// store payload in request (tmp) to make sure you can finish the request later on
				//provider.request.payload = payload;
				provider.request.isstored = true;
				
				// Add index to shares array to store with request. So we know what to share to data requester
				// add to shares array
				if (provider.request.shares == undefined || !Array.isArray(provider.request.shares))
					provider.request.shares = [];

				provider.request.shares.push({index : indexId} ); // add reference to index				
				
				// update the request in storage
				await db.set('requests', e.params['id'], provider.request);
						

                // screen wait helper: get the resttime
                var restTime = tools.timer.calculate(3000);

                window.setTimeout(function () {
                    // redirect to the request itself
                    e.redirect('#/request/' + e.params['id']);
                }, restTime);


            }

            runasync();

        });
    },

    // The request is being completed, now time to send the payload to provider and update state
    getcompleterequest: function (e) {

        e.partial('views/request_processing.tpl', null, function (t) {

            tools.setTheme('grey');
            tools.setLayout('wizard');

            // screen wait helper: start the time
            tools.timer.start();

            async function runasync() {

				// 1: get index from request
				let request = await db.get('requests', e.params['id']);

				// 1a get personal pgp keypair
				let pgpKeyPair = await db.get('app','pgpkey');

				// 2... contractkey -> dit wordt de public key van volksbank
				
				// 3: get docs from index
				for (var i = 0; i < request.shares.length; i++)
				{
					let index = await db.get('index', request.shares[i].index);
					let encDoc = await ipfs.catJSON(index.hash);
					
					// 4: decrypt each doc with private key
					const options = {
						message: await openpgp.message.readArmored(encDoc.content),    // parse armored message
						publicKeys: (await openpgp.key.readArmored(pgpKeyPair.public)).keys[0], // for verification (optional)
						privateKeys: [(await openpgp.key.readArmored(pgpKeyPair.private)).keys[0]]                                 // for decryption
					};
					
					let decDoc = await openpgp.decrypt(options);
					
					//console.log(provider);
					//return;
					
					// 5: encrypt each doc with symmetric key (using public key provider)
					let msgOptions = {
					  message : openpgp.message.fromText(decDoc.data),
					  publicKeys: (await openpgp.key.readArmored(request.pubkey)).keys[0],  // input as String (or Uint8Array)
					  privateKeys: [(await openpgp.key.readArmored(pgpKeyPair.private)).keys[0]] // for signing
					};
				
					let encContractDoc = await openpgp.encrypt(msgOptions);
					
					// 6: store doc in IPFS 
					// actually optional for now: because the contract (with data) will be sent to provider
					// in future scenarios it is desired to keep the data yourself and only send the link to it - todo: deletions from ipfs for example! 
					let hash = await ipfs.addJSON({content : encContractDoc.data, version : '0.0.1'});
					//let hash = await context.setDoc({content : encContractDoc, version : '0.0.1'});
				
					// add hash to it in request to keep track
					request.shares[i].location = hash;
					
					// this also stores/sends the data (as long as IPFS/decentralised storage is not completed, we need to pass the data itself instead of the location)
					request.shares[i].fallback_value = JSON.parse(decDoc.data); 
				}		

				// 7: generate contract and store it locally
				await provider.init(request.settingsuri); // make sure provider is loaded
				const contractId = tools.uuidv4();
				let contract = {
					provider : provider.settings.name,
					date : new Date().toISOString(), //  ISO 8601 date and time
					shares : request.shares,
					version : '0.0.1'
				};	

				await db.set('contracts', contractId, contract);
				
				// share with provider ---------------------------------------
					
				// 1. format contract to send (remove things not to be shared)
				for (var i = 0; i < contract.shares.length; i++){
					// remove index from shares in contract, because thats only for the user
					delete contract.shares[i].index;
				}

				// 2: encrypt contract with with symmetric key (using public key provider)
				const contractStr = JSON.stringify(contract);				

				let msgOptions = {
				  message : openpgp.message.fromText(contractStr),
				  publicKeys: (await openpgp.key.readArmored(request.pubkey)).keys[0],  // input as String (or Uint8Array)
				  privateKeys: [(await openpgp.key.readArmored(pgpKeyPair.private)).keys[0]] // for signing
				};
				
				let contractEnc = await openpgp.encrypt(msgOptions);

				const payload = {
					contract : contractEnc.data,
					signingKey : pgpKeyPair.public
				};

				tools.log('Final contract payload:');
				tools.log(JSON.stringify(payload));

                // send payload
                var url = provider.settings.endpoints.return;
                url = url.replace('[token]', encodeURIComponent(provider.request.token));
                url = url.replace('[payload]', encodeURIComponent(provider.request.payload));

                // post data to provider
				$.ajax({
					type: 'POST',
					url: url,
					data: {token: provider.request.token, payload: encodeURIComponent(provider.request.payload), contract : JSON.stringify(payload)},    
					timeout: 120*1000,
					/*
					error: function(jqXHR, textStatus, errorThrown) {
						if(textStatus === 'timeout') {
						   //do something on timeout
						   tools.log('A timeout occured when posting data and finalizing request share...');
						}*/ 
					})
					
					// when data posted
					.done(function(data){
						
						/*
					// update state to data requester / trigger that package 
					var stateUrl = provider.settings.endpoints.state;
					stateUrl = stateUrl.replace('[token]', encodeURIComponent(provider.request.token));
					stateUrl = stateUrl.replace('[state]', encodeURIComponent('connected'));
					$.get(stateUrl);						
						*/
						tools.log('request shared');

						// screen wait helper: get rest time
						var restTime = tools.timer.calculate(3000);

						// show overlay that it's done
						window.setTimeout(function () {

							// completed, show overlay
							$('#doneOverlay').addClass('show');

							// redirect
							window.setTimeout(function () {
								e.redirect('#/request_done/' + e.params['id']);
							}, 3000);
						}, restTime);						
					});				
				
				/*
                $.post(url, { token: provider.request.token, payload: encodeURIComponent(provider.request.payload), contract : JSON.stringify(payload) }, function (data) {

                    //$.get(url, function(data){

                    tools.log('request shared');

                    // screen wait helper: get rest time
                    var restTime = tools.timer.calculate(3000);

                    // show overlay that it's done
                    window.setTimeout(function () {

                        // completed, show overlay
                        $('#doneOverlay').addClass('show');

                        // redirect
                        window.setTimeout(function () {
                            e.redirect('#/request_done/' + e.params['id']);
                        }, 3000);
                    }, restTime);

                });*/
            }

            runasync();

        });
    },

    getrequestdone : function (e) {

        // delete open request
        //context.removeItem('requests', e.params['id']);

        e.title('Dataverzoek afgerond');

        e.partial('views/request_done.tpl', null, function () {


            tools.setTheme('grey');
            tools.setLayout('app');

            async function runasync() {
                let item = await db.get('requests', e.params['id']);

                $('#companyname').text(tools.uppercaseFirstChar(item.name));

                // delete open request to wrap things up
                db.remove('requests', e.params['id']);
            }

            runasync();
        });

    },

    // show all shares (processed requests / contracts)
    getshares: function (e) {
        e.partial('views/shares.tpl', null, function () {
            tools.setTheme('grey');
            tools.setLayout('wizard');
        });
    }

}
