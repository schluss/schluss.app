global.tools = require('../tools.js');

function renderDatacard(id, name, description, logourl)
{

	var container = document.createElement("div");
	container.classList.add('card');
	container.addEventListener('click', function(){
		window.location.href = '#/data/card/' + id;
	});	

	var row = document.createElement("div");
	row.classList.add('row');
	
	var logocol = document.createElement("div");
	logocol.classList.add('col','sixth');
	logocol.setAttribute('style','padding:5px;');
	
	if (logourl != '')
		logocol.innerHTML = '<img src="'+logourl+'" alt="'+name+' logo" style="max-width:100%"/>';
	
	var txtcol = document.createElement("div");
	txtcol.classList.add('col','twothirds');
	txtcol.innerHTML = '<b>' + name + '</b><br/>' + description;
	
	var btncol = document.createElement("div");
	btncol.classList.add('col','sixth', 'right');
	btncol.innerHTML = '<img src="/assets/img/arrow-pink-right.svg" alt="arrow" />';	

/*
	var btn = document.createElement("button");
	btn.innerHTML = '<img src="/assets/img/arrow-white-right.svg" alt="arrow pointing right" />';
	btn.classList.add('round');
	//btn.href = action;
	btn.onclick = function () { window.location.href = action; };
	*/
		
	row.appendChild(logocol);
	row.appendChild(txtcol);
	row.appendChild(btncol);
	
	container.appendChild(row);

	document.getElementById('dataArea').appendChild(container);
	

}


module.exports = {
	
	// views:
	
	getdata : function(e) {
		
		e.partial('views/data.tpl', null, function(t){
			
			tools.setTheme('grey');
			tools.setLayout('app');				
			
			async function runasync()
			{
				// render notifications
				db.iterate('index', function(item, key){
					renderDatacard(key, item.name, item.description, item.logourl);
				});
				
				/*
				global.context.getItems('index', function(items, keys){

					var arr = [];
					
					// make array, if its a single result, its a 
					if (items.length == undefined){
						items.key = keys;
						arr.push(items);
					}
					else
					{
						arr = items;
						
						for (var i = 0; i <= arr.length; i++){
							//arr[i].key = keys[i].
							// TODO THIS, I DO NOT KNOW YET
						}
						
					}

					for (var i = 0; i < arr.length; i++){

						renderDatacard(arr[i].key, arr[i].name, arr[i].description, arr[i].logourl);
					}
					
				});
				*/

			}
			
			runasync();
		});
	},
	
	getcard : function(e) {
		
		async function runasync(){
			
			// get index
			db.get('index', e.params['id'])
				.then(async function(index){

					//tools.log(index);

					// get content
					let doc = await ipfs.catJSON(index.hash);
					
					
					
					// Decrypt the actual data
					let pgpKeyPair = await db.get('app','pgpkey');
					
					const options = {
						message: await openpgp.message.readArmored(doc.content),    // parse armored message
						publicKeys: (await openpgp.key.readArmored(pgpKeyPair.public)).keys[0], // for verification (optional)
						privateKeys: [(await openpgp.key.readArmored(pgpKeyPair.private)).keys[0]]                                 // for decryption
					};
					
					let payloadDecrypted = await openpgp.decrypt(options);
				
					var payload = payloadDecrypted.data; // contains the actual decrypted data					
					
					//tools.log(payload);
					
					var docJSON = JSON.parse(payload);
					
					var content = '';
					
					for (var i = 0; i < docJSON.length; i++){
						
						content += '<div>'+docJSON[i].label+'<br/><b>'+docJSON[i].value+'</b></div>';
						
					}
					
					//document.getElementById('cardContent').innerHTML = content;
					
					await e.partial('views/card.tpl', { index : index, doc : docJSON, content : content }, function(t){
						tools.setTheme('grey');
						tools.setLayout('app');		

					});								
			});
		}
		
		runasync();	
	}	
}