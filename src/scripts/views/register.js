global.tools = require('../tools.js')
global.openpgp = require('openpgp')

module.exports = {
		// PUBLIC
		getregister : function(e) {	
			
			e.partial('views/register.tpl', null, function(t){
				
				tools.setTheme('pink');
				tools.setLayout('wizard');
			
				// add / enable tooltip for ttip element
				tools.tooltip('ttip');
			
				var pincode = '';
				var pinInstance = $('#pinwrapper').pinlogin({
					placeholder : '*',
					reset : false,
					complete : function(pin){
	
						pincode = pin;
						
						// focus repeat instance 
						pinrepeatInstance.focus(0);
						
						// disable this instance
						pinInstance.disable();					
					}	
				});
				
				var pinrepeatInstance = $('#pinrepeatwrapper').pinlogin({
					placeholder : '*',
					reset : false,
					autofocus : false,
					complete : function(pin){
						
						if (pincode != pin)
						{
							pincode = '';
							
							// reset both instances
							pinInstance.reset();
							pinrepeatInstance.reset();
							
							// disable repeat instance
							pinrepeatInstance.disable(); 
							
							alert ('Pincode komt niet overeen, probeer het opnieuw');
						}
						else
						{
							// set pincode in hidden input
							$('#pin').val(pin);
							
							// disable both instances
							pinInstance.disable();
							pinrepeatInstance.disable(); 
							
							$('#registerSubmit')
								.attr('disabled',false)
								.focus()
								.click(function(){
									$('#registerForm').submit();
								});
						}
					}
				});	

				// disable repeat instance at start
				//( Commented this part due to issues in Browser. Need to recheck)
				pinrepeatInstance.disable();
				
			});
		},
	
		postregister : function(e){
			
			e.partial('views/register-wait.tpl', null, function(t){
				
				tools.setTheme('grey');
				tools.setLayout('wizard');
				
				// screen wait helper: start the timer
				tools.timer.start();					
				
				/// begin
				const _C = document.querySelector('.panelcontainer'), 
					  N = _C.children.length;

				let i = 0, x0 = null, locked = false, w, last = -1;

				function unify(e) {	return e.changedTouches ? e.changedTouches[0] : e };

				function lock(e) {
				  x0 = unify(e).clientX;
					_C.classList.toggle('smooth', !(locked = true))
				};

				function drag(e) {
					//e.preventDefault();
					
					if(locked) 		
						_C.style.setProperty('--tx', `${Math.round(unify(e).clientX - x0)}px`)
				};

				function move(e) {
				  if(locked) {
					let dx = unify(e).clientX - x0, s = Math.sign(dx), 
								f = +(s*dx/w).toFixed(2);

					if((i > 0 || s < 0) && (i < N - 1 || s > 0) && f > .2) {
							_C.style.setProperty('--i', i -= s);
							f = 1 - f
						}
						tools.log(i);
						activate(i);
					_C.style.setProperty('--tx', '0px');
						_C.style.setProperty('--f', f);
					_C.classList.toggle('smooth', !(locked = false));
					x0 = null
				  }
				};

				function size() { w = window.innerWidth };

				size();
				_C.style.setProperty('--n', N);

				addEventListener('resize', size, false);

				_C.addEventListener('mousedown', lock, false);
				_C.addEventListener('touchstart', lock, {passive: true});

				_C.addEventListener('mousemove', drag, false);
				_C.addEventListener('touchmove', drag, {passive: true});

				_C.addEventListener('mouseup', move, false);
				_C.addEventListener('touchend', move, false);
				
				//document.getElementById('prev').addEventListener('click', prev, false);
				//document.getElementById('next').addEventListener('click', next, false);
				
				activate(0);
				
				function activate(bulletnr){
					$('#progress' + bulletnr).toggleClass('on');
					
					if (last != -1)
						$('#progress' + last).toggleClass('on');
					
					last = bulletnr;
				}
				/// end template javascript code
				

				promiseWorker.postMessage({action : 'register', params : e.params['pin']})
					.then(async function(){				
			
						await db.set('app','app', {version : '1.0', name : 'Schluss', displayname : 'Anoniempje'});
						
						//await db.set('app', 'key', result.data.key);
						
						pubsub.publish('onRegisterSuccess');
						
						
						// screen wait helper: get the resttime
						var restTime = tools.timer.calculate(10000);	
						
						window.setTimeout(function(){
							
							// vault created, show overlay
							$('#doneOverlay').addClass('show');
							
							// Redirect to login (no more autologin, this is to force the user to enter the pincode again = better remembering)
							window.setTimeout(function(){ 
							
								tools.onetimemessage.set('firsttimelogin', true);
								////tools.log('GA NU REDIRECTEN');
								//window.location.href = '#/login';
							
								e.redirect('#/login');
							}, 3000);							
							
						}, restTime);
					
					});
				
				runAsync();
				
				async function runAsync()
				{
					// create PGP key pair
					var options = {
						userIds: [{ name:'xxx', email:'xx@xx.xx' }], // multiple user IDs
						numBits: 4096,                                            // RSA key size
						passphrase: ''         // protects the private key
					};	

					let key = await openpgp.generateKey(options);
					

					// store public key in ipfs (for when a third party needs users' public key to encrypt a message for that user)
					let pubKeyHash = await ipfs.add(key.publicKeyArmored);
					
					// store key in indexeddb
					await db.set('app','pgpkey', {'public' : key.publicKeyArmored, 'private' : key.privateKeyArmored, revoke : key.revocationCertificate, pubkeyhash : pubKeyHash });			
					
				}
				
			});
			
			
		}

}