/// IDIN config params -------------------

const idinApiUrl = 'https://idintest.schluss.app/api.php/api';
const idinApiConfigUrl = 'https://beta.schluss.app/idintest.schluss.app.config.json'; // must become https://idintest.schluss.app/config.json
const idinScope = 'idin_id';

// Register routes
pubsub.subscribe('onRegisterRoutes', function(callback){

	var idinRoutes = [
			['get', '#/idin/about', idin.getidin_about],
			['get', '#/idin/select-bank', idin.getidin_select_bank]
		];

	callback(idinRoutes);
});


// hook into onRegisterSuccess event to add a notification
pubsub.subscribe('onRegisterSuccess', function(){

	async function runAsync(){
		var notifications = [];
		notifications.push({
			id : 'idin_tip',	
			type : 'tip', 
			title : '',
			text : '<h3><span class="highlight"><b>Tip:</b></span> Plaats je identiteit in de kluis!</h3>',
			action : '#/idin/about'
			});
	
		await db.set('app', 'notifications', notifications);
	}
	
	runAsync();
});	


// Declare Views
var idin = {	
		/// IDIN routes -------------------
		
		getidin_about : function(e) {
			
			e.partial('views/idin/about.tpl', null, function(t){
                tools.setTheme('grey');
                tools.setLayout('wizard');
			});
		},
		
		getidin_select_bank : function(e) {

			e.partial('views/idin/select-bank.tpl', null, function(t){
				tools.setTheme('grey');
				tools.setLayout('app');	
				
				// get banks
				tools.xhr.getJSON(idinApiUrl + '/issuers').then(function(result){
					
					// fill select list options
					for (var i = 0; i < result.data[0].issuers.length; i++){	
						var bank = result.data[0].issuers[i];

						var opt = $('<option value="'+ bank.bank_id +'">'+ bank.bank_name +'</option>');
						opt.data('image', bank.bank_name);
						$('#bankSelect').append(opt)
					}
					
					// style selectbox
					tools.smartSelect('bankSelectContainer');					
				});
				
				/*
				// get banks
				$.ajax({
					cache: false,
					url: idinApiUrl + '/issuers',
					dataType: "json",
					success: function(result) {
						
						tools.log(result);

						for (var i = 0; i < result.data[0].issuers.length; i++){	
							var bank = result.data[0].issuers[i];
							tools.log(bank);
							var opt = $('<option value="'+ bank.bank_id +'">'+ bank.bank_name +'</option>');
							opt.data('image', bank.bank_name);
							$('#bankSelect').append(opt)
						}
						
						// style selectbox
						tools.smartSelect('bankSelectContainer');
					}
				});				
				*/
				//$.getJSON('https://idintest.schluss.app/api.php/api/issuers', function (result) {
				//});
				
				
				// temp, to test while api not working, when it works, uncomment the above one and remove this
				//tools.smartSelect('bankSelectContainer');
				
				// when bank selected
				$('#bankSelect').on('change', async function(){

					if (this.value == 0)
						return;
				
					// screen wait helper: start the time
					tools.timer.start();	

				   // show overlay that user s beeing redirect externally
					$('#redirectOverlay').addClass('show');

					$('#redirecturlname').text(tools.shorten(idinApiUrl, 30));
					
					// get config.json
					let cfg = await $.getJSON(idinApiConfigUrl);
					
					// create a request

					var key = tools.uuidv4();
					//var token = tools.uuidv4();

					// make redirect url
					var rUrl = cfg.endpoints.connect.replace('[token]', encodeURIComponent(key));
					rUrl = rUrl.replace('[returnurl]', encodeURIComponent(window.location.origin + '#/returnrequest/' + key));
					
					// uncomment this in live environment
					//rUrl = rUrl.replace('[settingsurl]', encodeURIComponent(window.location.origin + '/config.json'));
					
					// comment this in live environment!
					rUrl = rUrl.replace('[settingsurl]', encodeURIComponent('https://beta.schluss.app/config.json'));
					
					
					rUrl = rUrl.replace('[scope]', encodeURIComponent(idinScope));
					rUrl = rUrl.replace('[bankid]', encodeURIComponent(this.value));

					// save it in db	

					await db.set('requests', key, { token: key, name: cfg.name, settingsuri: window.location.origin + '/config.json', redirecturi : rUrl, scope: idinScope });
					tools.log('url:' + rUrl);

					// remove notification from storage
					db.get('app', 'notifications').then(function(n){
						
						var newList = [];
						
						for (var i = 0; i < n.length; i++){
							if (n[i].id != 'idin_tip')
								newList.push(n[i]);
						}
						
						db.set('app', 'notifications', newList);
						
						
					});
					

					// screen wait helper: get the resttime
					var restTime = tools.timer.calculate(3000);
															
					window.setTimeout(function () {
 						// redirect to api to continue. Api will redirect further to bank site
						window.location.href = rUrl;
					}, restTime);						
					
					/*
					console.log('test!');
					$.post('https://idintest.schluss.app/api/transactions', {
							issuer_id : this.value,
							lang : 'en',
							expiration_period : 'PT5M',
							entrance_code : 'test',
							merchant_reference : 'test121212',
							requested_service_id : 21974,
							assurance_level : 'nl:bvn:bankid:1.0:loa3',
							document_id : ''})
						.done(function (result) {
						
							console.log('result:');
							console.log(result);
							//return;
						
                            // show overlay that user s beeing redirect externally
                            $('#redirectOverlay').addClass('show');

                            $('#redirecturlname').text(tools.shorten(result.data[0].issuer_authentication_url, 30));
						
						
							// todo: save transaction_id?? not sure, but might need it later on
						
							// screen wait helper: get the resttime
							var restTime = tools.timer.calculate(3000);
																	
							window.setTimeout(function () {
								// redirect to bank site to further process the idin request
								window.location.href = result.data[0].issuer_authentication_url;
							}, restTime);									

							
						}).fail(function(error) {
						//alert( "error" );
						console.log(error);
					  })
					 */

				});
				
			});
			

			
		}

/*
		getidin_select_bank_redirect : function(e) {
			
			var bankId = e.params['bankid'];
			
			tools.log()
			
		});		*/
    }	