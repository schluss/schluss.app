global.entity = require('../entity.js');
global.tools = require('../tools.js');

module.exports = {
	// shows pincode login view. 
	// if type=firsttime display a custom text header
	getlogin : function(e) {
		
		//if 
		//console.log(url.searchParams.get("firsttime"));
		
		this.title('Inloggen');
		
		tools.setTheme('pink');
		tools.setLayout('wizard');			
		
		e.partial('views/login.tpl', null, function(t){
			
			// when first login, custom header
			if (tools.onetimemessage.get('firsttimelogin') === true)
				$('#loginTitle').text('Hoera! Je bent nu een echte Schlussie.');
			
			var pinInstance = $('#pinwrapper').pinlogin({
				placeholder : '*',
				complete : function(pin){

					$('#pin').val(pin);
					
					$('#loginForm').submit();					
				}	
			});
			
		});
	},
	
	postlogin : function(e){
		
		entity.login(e.params['pin'], function(result){

			if (result){
				e.trigger('onAppLoad', e);	// trigger app loaded event manually
				e.redirect('#/');
			}
			else {
				// todo, make nice flash message
				alert ('Ongeldige pincode, probeer het opnieuw');
			}
			
		});			
	},
	
	getlogout : function(e) {
		
		// logout user
		entity.logout(function(){
			
			// run unload tasks
			e.trigger('onAppUnload');
			
			// redirect back
			e.redirect('#/');
		});
	}
}