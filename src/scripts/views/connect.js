global.tools = require('../tools.js')
global.provider = require('../provider.js');

module.exports = {
    // PUBLIC: handle, initialize and redirect 3rd party connection requests
    getconnect: function (e) {

        e.partial('views/request_loading.tpl', null, function () {

            tools.setTheme('grey');
            tools.setLayout('wizard');

            // screen wait helper: start the time
            tools.timer.start();

			async function runasync(){
					
				
				$.getJSON(e.params['settingsuri'], async function(result){
					
					// get provider public key
					$.get(result.pubkeyurl, async function(pgppubkey){
						
						// get update api url from config.json. to do so, first load the json
						await provider.init(e.params['settingsuri']);
						
						// get details about the requested scope
						let scope = await provider.getScope(e.params['scope']);
							
							
						// generate unique hash (will be used as token and identifier for this request)
						var key = tools.uuidv4();
						
						// create the proper redirect-to-third party url. why now? because you can add additional parameters to the url at this point (used with the idin request for example with 'bankid'
						var redirectUrl = scope.settings.endpoints.connect.replace('[settingsurl]', encodeURIComponent(e.params['settingsuri']));
						redirectUrl = redirectUrl.replace('[scope]', encodeURIComponent(scope.scope));
						redirectUrl = redirectUrl.replace('[token]', encodeURIComponent(key)); // todo: make unique token for use only between schluss.app and third party (duo)
						redirectUrl = redirectUrl.replace('[returnurl]', encodeURIComponent(window.location.origin + '#/returnrequest/' + key)); // todo: make unique token for use only between schluss.app and third party (duo)

						// save it in db	
						await db.set('requests', key, { token : e.params['token'], name : result.name, settingsuri : e.params['settingsuri'], redirecturi : redirectUrl, scope : e.params['scope'], pubkey : pgppubkey});
						
						var stateUrl = provider.settings.endpoints.state;
						stateUrl = stateUrl.replace('[token]', encodeURIComponent(e.params['token']));
						stateUrl = stateUrl.replace('[state]', encodeURIComponent('connecting'));
					
						// update client api to 'connecting'
						e.trigger('connection-update-clientapi', { token :  e.params['token'], apiuri : stateUrl});
							
						// screen wait helper: get the resttime
						var restTime = tools.timer.calculate(3000);	

						window.setTimeout(function(){
							// redirect 'home' -> and automatic go to registration or login when needed
							e.redirect('#/');										
						}, restTime);
			
					});	
				});

			}
			
            runasync();
        });
    }
}
