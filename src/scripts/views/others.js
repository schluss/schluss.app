global.tools = require('../tools.js');

function renderNotification(type, title, text, action)
{
	switch(type){
	
		case  'tip' :
	
			var container = document.createElement("div");
			container.classList.add('card', 'tip');
			//container.classList.add('tip');
			
			var nrow = document.createElement("div");
			nrow.classList.add('row');
			
			var ncol1 = document.createElement("div");
			ncol1.classList.add('col','fivesixth');
			ncol1.innerHTML = text;
			
			var ncol2 = document.createElement("div");
			ncol2.classList.add('col','onesixth','right');
			ncol2.innerHTML = '<br/>';
	
			var btn = document.createElement("button");
			btn.innerHTML = '<img src="/assets/img/arrow-white-right.svg" alt="arrow pointing right" />';
			btn.classList.add('round');
			//btn.href = action;
			btn.onclick = function () { window.location.href = action; };
			
			ncol2.appendChild(btn);
			
			nrow.appendChild(ncol1);
			nrow.appendChild(ncol2);
			
			container.appendChild(nrow);
	
			document.getElementById('notificationArea').appendChild(container);
	
		break;
		
	}
		
}

module.exports = {
		
	// views:
	
		home : function(e) {
			
			e.partial('views/home.tpl', null, function(t){
				
				tools.setTheme('grey');
				tools.setLayout('app');				
				
				async function runasync()
				{
					let app = await db.get('app', 'app');
					
					// get the displayed name
					if (app.displayname == undefined)
						app.displayname = 'Anoniempje';
					
					document.getElementById('displayname').innerHTML = app.displayname;
					
					// render notifications
					let notifications = await db.get('app', 'notifications');
			
					for (var i = 0; i < notifications.length; i++)
						renderNotification(notifications[i].type, notifications[i].title, notifications[i].text, notifications[i].action);
					
				}
				
				runasync();

			});
		},
				
		// PUBLIC
		welcome : function(e){
			
			e.partial('views/welcome.tpl', null, function(a){
		
				tools.setTheme('white');
				tools.setLayout('wizard');

				const _C = document.querySelector('.panelcontainer'), 
					  N = _C.children.length;

				let i = 0, x0 = null, locked = false, w, last = -1;

				function unify(e) {	return e.changedTouches ? e.changedTouches[0] : e };

				function lock(e) {
				  x0 = unify(e).clientX;
					_C.classList.toggle('smooth', !(locked = true))
				};

				function drag(e) {
					//e.preventDefault();
					
					if(locked) 		
						_C.style.setProperty('--tx', `${Math.round(unify(e).clientX - x0)}px`)
				};

				function move(e) {
				  if(locked) {
					let dx = unify(e).clientX - x0, s = Math.sign(dx), 
								f = +(s*dx/w).toFixed(2);

					if((i > 0 || s < 0) && (i < N - 1 || s > 0) && f > .2) {
							_C.style.setProperty('--i', i -= s);
							f = 1 - f
						}
						tools.log(i);
						activate(i);
					_C.style.setProperty('--tx', '0px');
						_C.style.setProperty('--f', f);
					_C.classList.toggle('smooth', !(locked = false));
					x0 = null
				  }
				};

				function size() { w = window.innerWidth };

				size();
				_C.style.setProperty('--n', N);

				addEventListener('resize', size, false);

				_C.addEventListener('mousedown', lock, false);
				_C.addEventListener('touchstart', lock, {passive: true});

				_C.addEventListener('mousemove', drag, false);
				_C.addEventListener('touchmove', drag, {passive: true});

				_C.addEventListener('mouseup', move, false);
				_C.addEventListener('touchend', move, false);
				
				//document.getElementById('prev').addEventListener('click', prev, false);
				//document.getElementById('next').addEventListener('click', next, false);
				
				activate(0);
				
				function activate(bulletnr){
					$('#progress' + bulletnr).toggleClass('on');
					
					if (last != -1)
						$('#progress' + last).toggleClass('on');
					
					last = bulletnr;
				}
				
				function next()
				{
					i++;
					f=0;
					//_C.classList.toggle('smooth', !(locked = false));	
					_C.style.setProperty('--i', i);
					_C.style.setProperty('--f', 0);
					_C.style.setProperty('--tx', '0px');
					
								
				}
				
				function prev()
				{
					i--;
					f=0;
					//_C.classList.toggle('smooth', !(locked = false));
					_C.style.setProperty('--i', i);
					_C.style.setProperty('--f', 0);
					_C.style.setProperty('--tx', '0px');			
				}

			});			
		},	

		// PUBLIC
		ourpromise : function(e){
			
			e.partial('views/promise.tpl', function(t){
				tools.setTheme('grey');
				tools.setLayout('wizard');
			});	
        },
		
		terms : function(e){
			
			e.partial('views/terms.tpl', function(t){
				tools.setTheme('white');
				tools.setLayout('wizard');
			});	
		}	
}