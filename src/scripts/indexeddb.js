var localforage = require('localforage');

module.exports = {
			
	version : 1.0,
	instances : {},

	resetInstances	: function()
	{
		this.instances = {};
	},

	createInstance : async function(name){
		return this.instances[name] = localforage.createInstance({
			name 		: 'schluss',
			version     : this.version,
			storeName   : name,
		});	
	},

	getInstance : async function(name){
		if (!this.instanceCreated(name))
			await this.createInstance(name);

		await this.instances[name].ready(); // to be sure, wait for it to be ready to use!

		return this.instances[name];
	},
	
	instanceCreated : function(name){
		return this.instances[name] === 'undefined';
	},
	
	// CRUD operations
	
	get : async function(instanceName, key){
		return (await this.getInstance(instanceName, true)).getItem(key);
	},
	
	set : async function(instanceName, key, value){
		return (await this.getInstance(instanceName, true)).setItem(key, value);
	},
	
	remove : async function(instanceName, key){
		return (await this.getInstance(instanceName)).removeItem(key);
	},	

	count : async function(instanceName){
		return (await this.getInstance(instanceName)).length();
	},	
	
	// iterate records one by one
	iterate : async function(instanceName, iteratorCallback){
		return (await this.getInstance(instanceName)).iterate(function(value,key,iterationNumber){iteratorCallback(value,key,iterationNumber);});
	},
	
	// return the first item from the given table
	first : async function(instanceName){
		return (await this.getInstance(instanceName)).iterate(function(value,key,iterationNumber){
			return {key : key, value : value};
		}).catch(function(err) {
			console.log(err);
		});
	}
}