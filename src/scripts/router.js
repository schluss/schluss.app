views = require('./views.js');

module.exports = {

	getRoutes: function () {
		return [
			['get', '#/', views.others.home],
			['get', '#/welcome', views.others.welcome],
			['get', '#/our-promise', views.others.ourpromise],
			['get', '#/terms', views.others.terms],
			['get', '#/register', views.register.getregister],
			['post','#/register', views.register.postregister],
			['get', '#/login', views.auth.getlogin],
			['post','#/login', views.auth.postlogin],
			['get', '#/logout', views.auth.getlogout],
			['get', '#/request/:id', views.request.getrequest],
			//['get', '#/returnrequest/:id/:payload', views.request.getreturnrequest],
			['get', '#/returnrequest/:id', views.request.getreturnrequest],
			['get', '#/completerequest/:id', views.request.getcompleterequest],
			['get', '#/request_done/:id', views.request.getrequestdone],
			['get', '#/connect/:token/:settingsuri/:scope', views.connect.getconnect],
			['get', '#/shares', views.request.getshares],
			['get', '#/data', views.data.getdata],
			['get', '#/data/card/:id', views.data.getcard]
		];
	},
}
