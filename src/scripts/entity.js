var config = require('./config.js');

// object to keep track of the 'entity' (user) and session
// after one single page refresh, all of this is gone and user needs to login again!
// future: maybe allow storage of these in a cookie, to allow persistent sessions, for now this is a deliberate choice for improved security!
var entity = {
	
	authenticated : false,	// is the entity logged in
	pincodehash : '',		// for this session: the hash that decrypts the key
	key : '',				// contains the key while in a valid session
	
	login : function(pin, callback){
		
		// hash pin
		var pincodeHash = CryptoJS.SHA256(pin);
		
		entity.authenticate(pincodeHash.toString(), function(result){callback(result)});
	},
	// when persistentSession, authenticate using pincodehash
	authenticate : async function(pincodeHash, callback){

		let key = await db.get('app', 'key');

			try
			{
				var decryptedkey = CryptoJS.AES.decrypt(key, pincodeHash);
				
				if (decryptedkey == '')
					throw('invalid key');
				
				// try to decode - todo, create proper pincode hash comparison instead of 'just try to decrypt and error on fail'
				var k = decryptedkey.toString(CryptoJS.enc.Utf8);
				
				// all went well, save user in 'session'
				entity.authenticated = true;
				entity.pincodehash = pincodeHash;
				entity.key = k;
				
				// start session
				entity.session.start(pincodeHash);
				
				// when there's a persistent session store pincode hash in context
				if (config.persistentSession)
				{	
					await db.set('app','session', pincodeHash);
					callback(true);
					/*
					context.setItem('db','session', pincodeHash).then(function(){
						callback(true);
					});*/
				}
				else
					callback(true);
			}
			catch(e)
			{
				console.log(e);
				callback(false);
			}

/*
		context.getItem('db','key').then(function(key){
			
			try
			{
				var decryptedkey = CryptoJS.AES.decrypt(key, pincodeHash);
				
				if (decryptedkey == '')
					throw('invalid key');
				
				// try to decode - todo, create proper pincode hash comparison instead of 'just try to decrypt and error on fail'
				var k = decryptedkey.toString(CryptoJS.enc.Utf8);
				
				// all went well, save user in 'session'
				entity.authenticated = true;
				entity.pincodehash = pincodeHash;
				entity.key = k;
				
				// start session
				entity.session.start(pincodeHash);
				
				// when there's a persistent session store pincode hash in context
				if (config.persistentSession)
				{
					context.setItem('db','session', pincodeHash).then(function(){
						callback(true);
					});
				}
				else
					callback(true);
			}
			catch(e)
			{
				console.log(e);
				callback(false);
			}				
			
		});	*/		
	},
	logout : async function(callback){
		
		entity.authenticated = false;
		entity.pincodehash = '';
		entity.key = '';

		// remove persistentSession
		if (config.persistentSession)
		{
			db.remove('app','session').then(function(){
				callback();
			});
			
			//context.removeItem('db','session').then(function(){
			//	callback();
			//});
		}
		else
			callback();
	},
	
	/* Handling the entity (user) session 
	 * Call session.start([key]) once after login
	 * subscribe on session.onInvalid to get notified when session is invalid, you do so by:
		session.onInvalid = function(){ // handle / redirect here };
	*/
	session : {
		
		_sessTimer : null,
		_infocustimeout : 600, 	// session timeout time when the app has focus (in seconds)
		_timeout : 120,			// session timeout time when the app does not have focus (in seconds)
		
		onInvalid :  function(){},	// when the session is invalid, this method gets called
		
		// start a new session
		start : function(key, infocusTimeout = 600, timeout = 120){
			
			tools.log('session: start');
			
			this._infocustimeout = infocusTimeout;
			this._timeout = timeout;
			
			// set session key / token
			localStorage.setItem('sess_key', key);
			
			// add eventlisteners to measure session interacting things
			
			// when loosing focus
			window.addEventListener('blur', this.looseFocus);
			
			// when regaining focus / interacting with page
			window.addEventListener('focus', this.resume);
			window.addEventListener('mousedown', this.resume);
			window.addEventListener('touchstart', this.resume, {passive: true});
			
			// start interval to periodically check if session is still valid, check every minute
			this._sessTimer = setInterval(entity.session.isValid, 60000);
			
			// 'resume' session (for the first time in this case)
			this.resume();
		},
		
		// update an existing session by resetting the counter
		resume : function(){
			localStorage.setItem('sess_start', new Date());
			localStorage.setItem('sess_lostfocus', '0');
		},
		
		looseFocus : function(){
			localStorage.setItem('sess_lostfocus', '1');
		},
		
		// checks if the session is valid, if not clear the session
		isValid : function(){
			
			var now = new Date();
			var start = new Date(localStorage.getItem('sess_start'));
			var lostFocus = (localStorage.getItem('sess_lostfocus') === '1') ? true : false;
			
			// check if time has passed while not having focus
			if (lostFocus && (now - start) / 1000 > entity.session._timeout){
				
				tools.log('session: expired (inactivity timeout while window not in focus)');
				
				// kill session
				entity.session.destroy();
				
				// invoke invalid event
				entity.session.onInvalid();
				
				return false;
			}
			
			// check if time has passed while having focus
			else if(!lostFocus && (now - start) / 1000 > entity.session._infocustimeout){
				
				tools.log('session: expired (inactivity timeout while window in focus)');
				
				// kill session
				entity.session.destroy();
				
				// invoke invalid event
				entity.session.onInvalid();
				
				return false;
			}

			// all ok
			return true;
		},
		
		// kills the session and do a cleanup
		destroy : function(){tools.log('session:destroy');
			
			localStorage.removeItem('sess_start');
			localStorage.removeItem('sess_lostfocus');
			localStorage.removeItem('sess_key');
			
			// cleanup, clear interval and eventlisteners
			clearInterval(entity.session._sessTimer);
			window.removeEventListener('blur', this.looseFocus);
			window.removeEventListener('focus', this.resume);
			window.removeEventListener('mousedown', this.resume);
			window.removeEventListener('touchstart', this.resume, {passive: true});
		},
		
		// return the session token / key
		getKey : function()
		{
			// sanity check: session still active? Only then return the key
			if (this.isValid())
				return localStorage.getItem('sess_key');
		}
	}
};	
	
module.exports = entity;