/**
 * Schluss.app
 * Personal locker - Powered by Schluss technology
 *
 * @version 0.0.4
 * @author Schluss
 * @see <a href="https://www.schluss.org" target="_blank">www.schluss.org</a> 
 * @see <a href="https://gitlab.com/schluss/schluss.app" target="_blank">https://gitlab.com/schluss/schluss.app</a> 
 */

// jquery
window.$ = global.jQuery = require('jquery');

window.$.ajaxSetup({ cache: false }); // disable url caching

// jquery plugins:
require('jquery-pinlogin/src/jquery.pinlogin.js');

// config
const config = require('./config.js');

global.pubsub = require('./pubsub.js');

// routing / framework
var app = $.sammy = require('sammy');
require('sammy/lib/plugins/sammy.template.js');
require('sammy/lib/plugins/sammy.nested_params.js');
require('sammy/lib/plugins/sammy.flash.js');
require('sammy/lib/plugins/sammy.title.js');

// load dynamically loadable modules
require('./plugins/plugins.js');

// storage
//global.ipfs = require('./ipfs_minimock.js');
global.db = require('./indexeddb.js');
global.ipfs = require('./ipfs_minimock.js');

// crypto
global.CryptoJS = require('crypto-js');

// other modules
global.tools = require('./tools.js');
global.entity = require('./entity.js');
global.provider = require('./provider.js');
global.views = require('./views.js');
global.router = require('./router.js');

// Register worker with wrapper
var PromiseWorker = require('promise-worker');
global.promiseWorker = new PromiseWorker(new Worker('assets/js/worker.js'));

// sammy: routing and displaying views
var app = $.sammy('#main', function () {
//global.app = Sammy('#main', function () {

	/// VARS -------------------

	this.persistentSession = true;//config.persistentSession; // true; 	// true: browser refresh does not delete session, false: login after browser refresh

	this.use('Template', 'tpl');
	this.use('Flash');
	this.use('Title');
	this.use('NestedParams');

	this.setTitle('Mijn kluis - Powered by Schluss');

	this.debug = true; //config.debug;

	this.isLoaded = false;	// keeping track if app is loaded (first time loading)

	// handle session timeout event
	entity.session.onInvalid = function () {
		entity.logout(function () {
			window.location.href = '/'; //refresh window
		});
	};
	/// ROUTES -------------------
	this.mapRoutes(router.getRoutes());
	
	// To test diff screens
	this.get('#/test', function (e) {
		e.partial('views/request_loading.tpl', null, function () {
			tools.setTheme('grey');
			tools.setLayout('wizard');
		});
	});
});
	
// init pubsub mechanism to be able to dynamically add routes:
pubsub.publish("onRegisterRoutes", function(additionalRoutes){
	app.mapRoutes(additionalRoutes);		
});	


// Before every request is processed (further), do some checks!
app.around(function (callback) {

	var e = this;

	// always, exept login|register|terms|welcome|our-promise|connect
	// note: connect url does not match with contextMatchesOptions, so used a hack in the if...
	if (app.contextMatchesOptions(this, { except: { path: ['#/login', '#/register', '#/terms', '#/welcome', '#/our-promise', '#/connect'] } }) && this.path.indexOf('#/connect/') === -1) {

		// check if logged in
		if (!entity.authenticated) {
			if (app.persistentSession) {
				
				db.get('app', 'session').then(function(pincodeHash){

					if (pincodeHash != null) {
						
						entity.authenticate(pincodeHash, function () {
							
							e.trigger('onAppLoad', e);
							callback();
						});
					}
					else {
						e.redirect('#/login');
					}
				});
			}
			else {
				e.redirect('#/login');
			}
		}

		// logged in:
		else {
			e.trigger('onPageLoad');
			callback();
		}
	}

	// only: register
	else if (app.contextMatchesOptions(this, '#/register')) {

		// when already registered
		db.get('app','app').then(function(result){
			
			if (result !== null){
				e.redirect('#/');
			}
			else {
				callback();
			}
		});
	}

	// only: login
	else if (app.contextMatchesOptions(this, '#/login')) {

		// when already loggedin
		if (entity.authenticated) {
			e.redirect('#/');
		}
		else {
			
			db.get('app','app').then(function(result){
				
				// when not registered, show intro
				if (result === null)
					e.redirect('#/welcome');
				// show login
				else
					callback();
			});
		}
	}

	// all other cases
	else {
		callback();
	}

});

/// EVENTS -------------------

// runs every real page/browser refresh and after login
app.bind('onAppLoad', function(e, data){
	
	async function runAsync(e){
		tools.log('triggered: onAppLoad');

		// check for pending requests! (but only if we are not returning from a request (or starting a new request)!)
		if (data.path.indexOf('#/returnrequest/') === -1 && data.path.indexOf('#/connect/') === -1) {

			let request = await provider.getRequest();
			
			// automagically redirect to the request details page
			if (request != undefined) {
				tools.log('pending request found, loading...');
				data.redirect('#/request/' + request.key);
			}
		}
	}
	
	runAsync();
});

// runs on every page load
app.bind('onPageLoad', function(e, data){
	
	async function runAsync(){
		tools.log('triggered: onPageLoad');
	}
	
	runAsync();
});		

// when user logged / app is closed, do some unloading tasks
app.bind('onAppUnload', function(e, data){
	
	async function runAsync(){
		tools.log('triggered: onAppUnload');
	}
	
	runAsync();
});

// Update api of third party connection request by sending a request to apiuri
// call: trigger('connection-update-clientapi', { token : '', apiuri : '', action : 'connecting|connected'});
app.bind('connection-update-clientapi', function (e, data) {

	// todo: if appname is given (and not token+apiuri), get the needed details from requests table in storage

	// demo new
	//tools.log ('post to new demo api');
	$.get(data['apiuri'])
		.done(function (response) {
			tools.log('api returned:');
			tools.log(response);
		});
});

// run the default route
$(function () {
	app.run('#/');
});
