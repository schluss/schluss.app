var _ipfs = {
	
	provider : null,
	requestBase : null,
	
	setProvider : function(provider)
	{
		return;
	},
	
	hash : function(length) {
	  var txt = "";
	  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	  for (var i = 0; i < length; i++)
		txt += possible.charAt(Math.floor(Math.random() * possible.length));

	  return txt;
	},	
	
	// heette: addData, exposed als add
	add : function(input) {
		
		return new Promise(async function(resolve, reject){
		
			var hash = _ipfs.hash(50);
			localStorage.setItem('ipfsminimock_' + hash, JSON.stringify(input));
			resolve(hash);
		  
		});
	},

	addJSON : async function(jsonData) {
		return new Promise(async function(resolve, reject){
		  
			let result = await _ipfs.add(jsonData);
			resolve(result);
		
		});
	},	
	
	stat : async function(ipfsHash) {
		return new Promise(async function(resolve, reject){
		  resolve('');
		});
	},
	
	cat : function(ipfsHash) {
		return new Promise(async function(resolve, reject){
		  
			var item = localStorage.getItem('ipfsminimock_' +  ipfsHash);
			resolve(item);
	  
		});
	},
	
	catJSON : function(ipfsHash) {
		return new Promise(async function(resolve, reject){
			
			var item = JSON.parse(localStorage.getItem('ipfsminimock_' + ipfsHash));
			resolve(item);
		
		});
	}
};

module.exports = _ipfs;