// Worker for general usage

// config
var config = require('./config.js');
var registerPromiseWorker = require('promise-worker/register');


// crypto
global.CryptoJS = require('crypto-js');

global.db = require('./indexeddb.js');

global.tools = require('./tools.js');

// register different messages
registerPromiseWorker(function(message){
	
	switch(message.action)
	{
		// misc methods
		case 'register' : return worker.register(message.params);
			break;
	}
	
});


var worker = (function(){
	
	var x = {};
	
	x.register = async function(pin){
				
		// hash pin
		var pincodeHash = CryptoJS.SHA256(pin);
		
		// generate unique and random key
		var key = tools.hash(64); // 256bits hash
		
		// encrypt key with pincodeHash
		var encrypted = CryptoJS.AES.encrypt(key, pincodeHash.toString());

		// store encrypted key
		db.set('app','key', encrypted.toString())
			.then(function(){
				return true;
			});
		
		/*
		// create a new 'app' instance	
		db.set('db','app', {version : '1.0', name : 'Schluss'})
			.then(function(){
				
				// store key
				return db.set('db','key', encrypted.toString());
			})
			.then(function(){
				return true;
			});
*/			
	};	
	
	x.encrypt = async function(plaintext, password)
	{
		//const password = tools.hash(64); // 256bits hash as unique key
		
		const pwUtf8 = new TextEncoder().encode(password);                                 // encode password as UTF-8
		const pwHash = await crypto.subtle.digest('SHA-256', pwUtf8);                      // hash the password

		const iv = crypto.getRandomValues(new Uint8Array(12));                             // get 96-bit random iv

		const alg = { name: 'AES-GCM', iv: iv };                                           // specify algorithm to use

		const key = await crypto.subtle.importKey('raw', pwHash, alg, false, ['encrypt']); // generate key from pw

		const ptUint8 = new TextEncoder().encode(plaintext);                               // encode plaintext as UTF-8
		const ctBuffer = await crypto.subtle.encrypt(alg, key, ptUint8);                   // encrypt plaintext using key
	 
		const ctArray = Array.from(new Uint8Array(ctBuffer));                              // ciphertext as byte array
		const ctStr = ctArray.map(byte => String.fromCharCode(byte)).join('');             // ciphertext as string
		const ctBase64 = btoa(ctStr);                                                      // encode ciphertext as base64

		const ivHex = Array.from(iv).map(b => ('00' + b.toString(16)).slice(-2)).join(''); // iv as hex string

		//return ivHex+ctBase64;                                                             // return iv+ciphertext
	
		postMessage({action : 'encrypt', type: 'finished', data : ivHex+ctBase64, key : password});
	
	};
	
	x.decrypt = async function(ciphertext, password)
	{
		const pwUtf8 = new TextEncoder().encode(password);                                  // encode password as UTF-8
		const pwHash = await crypto.subtle.digest('SHA-256', pwUtf8);                       // hash the password

		const iv = ciphertext.slice(0,24).match(/.{2}/g).map(byte => parseInt(byte, 16));   // get iv from ciphertext

		const alg = { name: 'AES-GCM', iv: new Uint8Array(iv) };                            // specify algorithm to use

		const key = await crypto.subtle.importKey('raw', pwHash, alg, false, ['decrypt']);  // use pw to generate key

		const ctStr = atob(ciphertext.slice(24));                                           // decode base64 ciphertext
		const ctUint8 = new Uint8Array(ctStr.match(/[\s\S]/g).map(ch => ch.charCodeAt(0))); // ciphertext as Uint8Array
		// note: why doesn't ctUint8 = new TextEncoder().encode(ctStr) work?

		const plainBuffer = await crypto.subtle.decrypt(alg, key, ctUint8);                 // decrypt ciphertext using key
		const plaintext = new TextDecoder().decode(plainBuffer);                            // decode password from UTF-8

		//return plaintext;                                                                   // return the plaintext
		postMessage({action : 'decrypt', type: 'finished', data : plaintext});
	};
	
	return x;
}());