// config
const config = require('./config.js');

module.exports = {
	// Generate a 'random' hash with given length
	hash : function(length) {
	  var txt = "";
	  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	  for (var i = 0; i < length; i++)
		txt += possible.charAt(Math.floor(Math.random() * possible.length));

	  return txt;
	},
	// generate semi unique (g)uuid in javascript
	// source: https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
	uuidv4 : function() {
	  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	  });
	},
	
	// logger for debugging messages etc
	log : function(message)
	{
		if (config.debug)
		{
			console.log(message);
		}
	},
	
	currentTheme : '',
	currentLayout : '',
	
	// enable a theme color, values: default, app, pink, grey
	setTheme : function(theme)
	{
		$('body').removeClass(); // remove all classes
		
		this.currentTheme = theme;
		
		// add theme and layout class
		$('body').addClass(this.currentTheme + ' ' + this.currentLayout);		
	},
	
	// enable a layout, values: default, app, wizard
	setLayout : function(layout)
	{
		$('body').removeClass(); // remove all classes
		
		this.currentLayout = layout;
		
		// add theme and layout class
		$('body').addClass(this.currentTheme + ' ' + this.currentLayout);		
	},
	
	// shorten a string and add ... when longer than given length
	shorten : function(txt, length)
	{
		return (txt.length > length) ? txt.substr(0, length-1) + '...' : txt;			
	},
	
	// make the first character of a string uppercase
	uppercaseFirstChar : function(txt){
		
		if (txt == '')
			return '';
		
		return txt.charAt(0).toUpperCase() + txt.slice(1);
	},
	
	/* Helper to calculate the waiting left time given a minimal overall wait time 
	This helper is used to show a waiting screen for a minimal of x seconds for example, if the 
	operation in the background finishes early, the screen still waits until the minimal time is met.
	*/
	
	timer : {
		
		startTime : null,
		endTime: null,
		
		// start the timer
		start : function(){
			// sanity check: reset the endtime:
			this.endTime = null;
			
			this.startTime = new Date();
		},

		// optional: stop the timer
		end : function(){
			this.endTime = new Date();
		},

		// given the minWaitTime, returns the wait time left (or 0 if time's up)
		calculate : function(minWaitTime)
		{
			if (this.endTime === null)
				this.end();
			
			var restTime = minWaitTime - (this.endTime - this.startTime);

			// reset
			this.startTime = null;
			this.endTime = null;				
			
			return (restTime <= 0) ? 0 : restTime;
		}
	},
	
	/* Store a message that can be read once. After 'get' the message will be deleted */
	onetimemessage : {
		
		messages : [],
		
		// Set a one time message
		set : function(name, value)
		{
			this.messages[name] = value;
		},

		// Get a one time message
		get : function(name)
		{
			var m = this.messages[name];
			
			delete this.messages[name];
			
			return m;
		},
		
	},
	
	// bind a tooltip to given element.
	// example: <img id="elementId" src=".." data-tooltip="tooltip text" />
	// tools.tooltip('elementId')
	tooltip : function(element)
	{
		var tip = document.getElementById(element);
		
		tip.onPress = function(e)
		{
			var tiptext, tipclose;
			
			// clicked upon when tooltip is opened, close it
			if (tip.isOpen)
			{
				tip.classList.remove('open');
				tip.isOpen = false;
				return;
			}
			
			// when first opened, create the needed elements
			if (!tip.isCreated)
			{
				tiptext = document.createElement('div');
				tiptext.id = tip.id + '_tiptext';
				tiptext.innerText = tip.getAttribute('data-tooltip');
				tiptext.classList.add('tooltiptext');	

				tipclose = document.createElement('img');
				tipclose.id = tip.id + '_tipclose';
				tipclose.src = '/assets/img/abort-tooltip.svg';
				tipclose.classList.add('tooltipclose');	

				tiptext.appendChild(tipclose);
				tip.appendChild(tiptext);

				tip.isCreated = true;
			}
			
			// add open class
			tip.classList.add('open');
			
			e.preventDefault();
			e.stopPropagation();	

			tip.isOpen = true;
		}
		
		// attach event listeners

		// mouse
		tip.addEventListener('click', tip.onPress);
		
		// touch
		tip.addEventListener("touchend", function(e){ 
			e.preventDefault();
			tip.onPress(e);
		}, {passive: false});	
	},
	
	smartSelect : function(elementId){
		var x, i, j, selElmnt, a, b, c;
		
		/*look for any elements with the class "custom-select":*/
		x = document.getElementById(elementId);

		selElmnt = x.getElementsByTagName("select")[0];

		/*for each element, create a new DIV that will act as the selected item:*/
		a = document.createElement("DIV");
		a.setAttribute("class", "select-selected");
		a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
		x.appendChild(a);
		/*for each element, create a new DIV that will contain the option list:*/
		b = document.createElement("DIV");
		b.setAttribute("class", "select-items select-hide");
		for (j = 1; j < selElmnt.length; j++) {
			/*for each option in the original select element,
			create a new DIV that will act as an option item:*/
			c = document.createElement("DIV");
			c.innerHTML = selElmnt.options[j].innerHTML;
			c.addEventListener("click", function(e) {
				/*when an item is clicked, update the original select box,
				and the selected item:*/
				
				var y, i, k, s, h;
				s = this.parentNode.parentNode.getElementsByTagName("select")[0];
				h = this.parentNode.previousSibling;
				for (i = 0; i < s.length; i++) {
				  if (s.options[i].innerHTML == this.innerHTML) {
					s.selectedIndex = i;
					h.innerHTML = this.innerHTML;
					y = this.parentNode.getElementsByClassName("same-as-selected");
					for (k = 0; k < y.length; k++) {
					  y[k].removeAttribute("class");
					}
					this.setAttribute("class", "same-as-selected");
					
					// trigger change event manually
					s.dispatchEvent(new Event('change'));
					
					break;
				  }
				}
				h.click();
			});
			b.appendChild(c);
		}
		
		x.appendChild(b);
		
		a.addEventListener("click", function(e) {
		  /*when the select box is clicked, close any other select boxes,
		  and open/close the current select box:*/
		  e.stopPropagation();
		  //closeAllSelect(this);
		  
		  //this.classList.remove("select-arrow-active");
		  //this.classList.add("select-hide");
		  
		  this.nextSibling.classList.toggle("select-hide");
		  this.classList.toggle("select-arrow-active");
		  
		  //x.value = 
		});
		
		//document.addEventListener("click", closeAllSelect);
		
		//this.closeSelect : function(element){
			
		//}
	},
	
	xhr : {
		init : function (){
			if (typeof XMLHttpRequest !== 'undefined') {
				return new XMLHttpRequest();
			}
			// fallback
			var versions = [
				"MSXML2.XmlHttp.6.0",
				"MSXML2.XmlHttp.5.0",
				"MSXML2.XmlHttp.4.0",
				"MSXML2.XmlHttp.3.0",
				"MSXML2.XmlHttp.2.0",
				"Microsoft.XmlHttp"
			];

			var xhr;
			for (var i = 0; i < versions.length; i++) {
				try {
					xhr = new ActiveXObject(versions[i]);
					break;
				} 
				catch (e){
				}
			}
			return xhr;
		},
		
		send : function(url, data, async){ 
			
			var that = this; 
			
			return new Promise(function(resolve, reject){ 
			
				if (async === undefined)
					async = true;
			
				var xhr = that.init();

				xhr.open('POST', url, async);

				xhr.onreadystatechange = function () {
					if (xhr.readyState == 4) {
						if (xhr.status === 200)
							resolve(xhr.responseText);
						else
							reject(xhr.status);
					}
				};
					
				xhr.send(data);	
			});		
		},
		
		get : function(url, async){
			
			var that = this; 
			//console.log(this);
			
			return new Promise(function(resolve, reject){ 
				
				if (async === undefined)
					async = true;
				
				var xhr = that.init();
				xhr.open('GET', url, async);
		
				xhr.onreadystatechange = function () {
					if (xhr.readyState == 4) {
						if (xhr.status === 200)
							resolve(xhr.responseText);
						else
							reject(xhr.status);
					}
				};
					
				xhr.send();				
			});	
		},
		
		getJSON : async function(url, async)
		{
			var that = this;
			
			return new Promise(async function(resolve, reject){ 
				let plainData = await that.get(url, async);
				resolve(JSON.parse(plainData));
			});
		}
	},	
};