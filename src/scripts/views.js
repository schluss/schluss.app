module.exports = {
    register: require('./views/register.js'),
    auth: require('./views/auth.js'), //login logout etc
    connect: require('./views/connect.js'),
    request: require('./views/request.js'),
    others: require('./views/others.js'),  // home, our-promise, welcome, terms
    data: require('./views/data.js')
}
