/*
 * SplitChunksPlugin is enabled by default and replaced
 * deprecated CommonsChunkPlugin. It automatically identifies modules which
 * should be splitted of chunk by heuristics using module duplication count and
 * module category (i. e. node_modules). And splits the chunks…
 *
 * It is safe to remove "splitChunks" from the generated configuration
 * and was added as an educational example.
 *
 * https://webpack.js.org/plugins/split-chunks-plugin/
 *
 */

/*
 * We've enabled UglifyJSPlugin for you! This minifies your app
 * in order to load faster and run less javascript.
 *
 * https://github.com/webpack-contrib/uglifyjs-webpack-plugin
 *
 */
const webpack = require('webpack');
const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
	entry: {
		app: ['babel-polyfill', './src/scripts/app.js','sammy'],
		worker: ['babel-polyfill', './src/scripts/worker.js'],
	},
	output: {
		filename: 'assets/js/[name].js',
		globalObject: 'this'
	},
	devtool: "cheap-module-eval-source-map",
	module: {
		rules: [
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader?cacheDirectory',
					options: {
						presets: ['@babel/preset-env']
					}
				}
			},
			{
				test: /\.(scss|css)$/,
				use: [
					{
						loader: 'style-loader'
					},
					{
						loader: 'css-loader'
					},
					{
						loader: 'sass-loader'
					}
				]
			}
		]
	},
	mode: 'development',
	optimization: {
		minimizer: [
			new TerserPlugin({
				cache: true,
				parallel: 4,//Enable multi-process parallel running and set number of concurrent runs.
				sourceMap: true, // Must be set to true if using source-maps in production
				terserOptions: {
					// https://github.com/webpack-contrib/terser-webpack-plugin#terseroptions
				}
			}),
		],
	},
	plugins: [
		new htmlWebpackPlugin({
		  inject: false,
		  template:path.join(__dirname,'src','index.html')
		}),
		// copy static assets to dist folder
		new CopyWebpackPlugin([{
				from: path.join(__dirname, 'src'),
				to: path.join(__dirname, 'dist'),
				ignore: ['scripts/**/*'],

			}
        ])
	  ],
};
