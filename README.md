# Schluss.app

Schluss locker webapp; javascript runs client side code only, fully decentralized and distributed end to end encryped storage.   

## Installation  
For coding, debugging and releasing   

#### Requirements   

- NPM   
- NodeJS (already available after installing NPM)   

*Run*
>\> npm install   

## Local development

This project uses Webpack to simplify the developing and building process. It contains an local development https webserver with hot reloading (ignore the certificate warnings in your browser)   

### Build the code
Does all the necessary things to generate a complete and working version of the app in the `./dist` folder   
>\> npm run build   


### Start webserver   
To start the local embedded https webserver run the following command:
>\> npm run serve   

Your browser opens automatically and shows the index.html in the `./dist` folder   

### Pack and publish   
To make a production ready version, that can be found in the `./dist` folder, run:   
>\> npm run publish   